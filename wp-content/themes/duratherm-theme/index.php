<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package     WordPress
 * @subpackage  Hockey24x7 Theme
 * @since       Hockey24x7 Theme 1.0.0
 */

if (!class_exists('Timber')) {
  echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp/wp-admin/plugins.php#timber">/wp/wp-admin/plugins.php</a>';
  return;
}

$context = Timber::get_context();

//Get the Posts

$context['posts'] = Timber::get_posts(['posts_per_page' => 6, 'order' => 'ASC']);
$context['howtitle'] = 'Tip\'s & How-Tos';
// $context['wp_title'] .= 'Heat Transfer Fluids tips and how tos';
$context['title'] .= 'Heat Transfer Fluids tips and how tos';

// echo '<xmp>';print_r($context);die;

//Load Microsite links
$args = array('name' => 'microsites');
$context['microsites'] = Timber::get_posts( $args );

//Get the Footer Links
$context['products'] = json_decode(file_get_contents( get_template_directory() . '/assets/dist/data/products.json'));
$context['footer_links'] = array();
$x=0;
foreach( $context['products'] as $prod ) {
  if( in_array('heat-transfer', $prod->duratherm_fluid_type) ) {
    $context['footer_links'][$x]['name'] = $prod->name;
    $context['footer_links'][$x]['slug'] = $prod->slug;
    $x++;
  }
}
sort($context['footer_links']);

Timber::render('archive.twig', $context);
