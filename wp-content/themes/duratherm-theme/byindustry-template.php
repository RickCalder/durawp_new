<?php 
/* Template Name: By Industry Template */ 

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$context['products'] = json_decode(file_get_contents( get_template_directory() . '/assets/dist/data/products.json'));
$context['footer_links'] = array();
$x=0;
foreach( $context['products'] as $prod ) {
  if( in_array('heat-transfer', $prod->duratherm_fluid_type) ) {
    $context['footer_links'][$x]['name'] = $prod->name;
    $context['footer_links'][$x]['slug'] = $prod->slug;
    $x++;
  }
}
$args = array (
  'post_type' => 'page',
  'meta_key' => '_wp_page_template',
  'meta_value' => 'byindustry-template.php',
  'order' => 'ASC',
  'posts_per_page' => -1
);
$posts = Timber::get_posts( $args );
$context['posts'] = $posts;
//Load Microsite links
$args = array('name' => 'microsites');
$context['microsites'] = Timber::get_posts( $args );
// echo '<xmp>'; print_r($context['posts']);die;
Timber::render( 'template-industry.twig', $context );