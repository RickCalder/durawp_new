<?php 
/**
* dkpdf-header.php
* This template is used to display content in PDF Header
*
* Do not edit this template directly, 
* copy this template and paste in your theme inside a directory named dkpdf 
*/ 
?>

<?php 
  	global $post;
  	$pdf_header_image = sanitize_option( 'dkpdf_pdf_header_image', get_option( 'dkpdf_pdf_header_image' ) );
    $pdf_header_image_attachment = wp_get_attachment_image_src( $pdf_header_image, 'full' );
    $pdf_header_show_title = sanitize_option( 'dkpdf_pdf_header_show_title', get_option( 'dkpdf_pdf_header_show_title' ) );
    $pdf_header_show_pagination = sanitize_option( 'dkpdf_pdf_header_show_pagination', get_option( 'dkpdf_pdf_header_show_pagination' ) );
?>

<?php
	// only enter here if any of the settings exists
	if( $pdf_header_image || $pdf_header_show_title || $pdf_header_show_pagination ) { ?>

		<div style="width:100%;">

			<?php
				// check if Header logo exists
				if( $pdf_header_image_attachment ) { ?>

					<div>
						<img style="width:auto;height:55px;display: block" src="<?php echo $pdf_header_image_attachment[0];?>">
						
						<h2 style="margin-top: 30px"><?php echo apply_filters( 'dkpdf_header_title', get_the_title( $post->ID ) ); ?></h2>
					</div>

				<?php } 

			?>



		</div>

	<?php }

?>



