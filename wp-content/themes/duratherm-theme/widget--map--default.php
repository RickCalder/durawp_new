<?php
/**
 * The template for displaying a map.
 *
 * @package     WordPress
 * @subpackage  Hockey24x7 Theme
 * @since       Hockey24x7 Theme 1.0.0
 */

if (!class_exists('Timber')) {
  echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
  return;
}

$context = Timber::get_context();
$context['latitude'] = $latitude;   // Setup by widget
$context['longitude'] = $longitude; // ...
$context['zoom'] = $zoom;           // ...
$context['width'] = $width;         // ...
$context['height'] = $height;       // ...

Timber::render(array('widget--map--default.twig'), $context);