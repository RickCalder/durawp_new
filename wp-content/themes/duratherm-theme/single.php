<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package     WordPress
 * @subpackage  Hockey24x7 Theme
 * @since       Hockey24x7 Theme 1.0.0
 */

if (!class_exists('Timber')) {
  echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp/wp-admin/plugins.php#timber">/wp/wp-admin/plugins.php</a>';
  return;
}

$root = $_SERVER['DOCUMENT_ROOT'] .'/wp-content/themes/duratherm-theme/';

$context = Timber::get_context();
$post = Timber::query_post();
$previous_post = get_previous_post();
$next_post = get_next_post();
$context['post'] = $post;
// $context['wp_title'] .= ' - ' . $post->title();
$context['howtitle'] = 'Tip\'s & How-Tos';
$context['comment_form'] = TimberHelper::get_comment_form();
$context['sharing'] = sharing_display();
$all_product_data = json_decode(file_get_contents($root . 'assets/dist/data/products.json'));
foreach($all_product_data as $product) {
  if($product->slug === $post->slug) {
    $context['current_product'] = $product;
    break;
  }
}
// echo '<xmp>'; print_r($context['current_product']); die;
$context['footer_links'] = array();
$x=0;
foreach( $all_product_data as $prod ) {
  if( in_array('heat-transfer', $prod->duratherm_fluid_type) ) {
    $context['footer_links'][$x]['name'] = $prod->name;
    $context['footer_links'][$x]['slug'] = $prod->slug;
    $x++;
  }
}
sort($context['footer_links']);
// echo '<xmp>';print_r($context['footer_links']);die;
if(isset($context['current_product'])) {
  $context['temp_array'] = explode(',', $context['current_product']->temperature_range);
  $context['temp_array_standard'] = [];
  for($i=0; $i< count($context['temp_array']); $i++) {
    $context['temp_array_standard'][$i] = round(($context['temp_array'][$i] * 1.8) +32);
  }
  // echo '<xmp>'; print_r($context['temp_array_standard']);print_r($context['temp_array']);die;
  $context['visc_array'] = explode(',', $context['current_product']->viscosity_range);
}
//Load Microsite links
$args = array('name' => 'microsites');
$context['microsites'] = Timber::get_posts( $args );

// Load Products
$args = array( 'post_type' => 'products', 'numberposts' => -1, 'order'=> 'ASC', 'orderby' => 'post_title','suppress_filters' => false);
$products = Timber::get_posts( $args );
$duraProducts = [];
foreach( $products as $product ) {
  if( $product->duratherm_product[0] == 'yes' ) {
    array_push($duraProducts, $product );
  }
}
$context['products'] = $duraProducts;
// echo '<xmp>';print_r($context['products']); die;
// echo '<xmp>';print_r($products); die;

//load testimonials
global $sitepress;
$lang='en';
$args = array('post_type' => 'testimonials', 'numberposts' => -1, 'suppress_filters' => false);
$context['testimonials'] = Timber::get_posts( $args );
// echo '<xmp>';print_r($context['testimonials']); die;

if ($previous_post) {
  $context['previous_post'] = new TimberPost($previous_post->ID);
}

if ($next_post && $next_post->ID != $post->ID) {
  $context['next_post'] = new TimberPost($next_post->ID);
}

$blog_ID = get_option('page_for_posts');
$context['blog_page'] = new TimberPost($blog_ID);



$context['post_pdf_link'] = get_field('post_pdf');
// echo '<xmp>'; print_r($context); die;
if (post_password_required($post->ID)) {
  Timber::render('single-password.twig', $context);
} else {
  Timber::render(array('single-' . $post->ID . '.twig', 'single-' . $post->post_type . '.twig', 'single.twig'), $context);
}
