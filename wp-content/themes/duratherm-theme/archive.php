<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package     WordPress
 * @subpackage  Hockey24x7 Theme
 * @since       Hockey24x7 Theme 1.0.0
 */

if (!class_exists('Timber')) {
  echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp/wp-admin/plugins.php#timber">/wp/wp-admin/plugins.php</a>';
  return;
}
$data = Timber::get_context();
$data['howtitle'] = 'Tip\'s & How-Tos';

$data['cat'] = strtolower( str_replace(' ', '-',single_cat_title('', false)));

$data['title'] = single_cat_title('', false);

$data['posts'] = Timber::get_posts( ['order' => 'ASC', 'category_name' => single_cat_title('', false)] );
global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}

// $args = array('name' => 'products');

// $data['post'] =Timber::query_post($args);


//Load Microsite links
$args = array('name' => 'microsites');
$data['microsites'] = Timber::get_posts( $args );

// Load Products
$data['products'] = json_decode(file_get_contents( get_template_directory() . '/assets/dist/data/products.json'));
$data['footer_links'] = array();
$x=0;
foreach( $data['products'] as $prod ) {
  if( in_array('heat-transfer', $prod->duratherm_fluid_type) ) {
    $data['footer_links'][$x]['name'] = $prod->name;
    $data['footer_links'][$x]['slug'] = $prod->slug;
    $x++;
  }
}
sort($data['footer_links']);
$data['post_count'] = $wp_query->found_posts;
$data['pagination'] = Timber::get_pagination(3);

// Templates to try to render
$templates = array('archive.twig', 'index.twig');
if (isset($params['base_route'])) {
  array_unshift($templates, 'page-' . $params['base_route'] . '.twig');
} else if( is_page('products')) {
  array_unshift($templates, 'archive-products.twig');
} else if (is_category()) {
  array_unshift($templates, 'archive-' . get_query_var('cat') . '.twig');
} else if (is_post_type_archive()) {
  array_unshift($templates, 'archive-products.twig');
}

$blog_ID = get_option('page_for_posts');
$data['blog_page'] = new TimberPost($blog_ID);
$data['products_page'] = new TimberPost(2389);

// echo '<xmp>';print_r($context['products']);die;

Timber::render($templates, $data);
