<?php

if (!class_exists('Timber')) {
  echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp/wp-admin/plugins.php#timber">/wp/wp-admin/plugins.php</a>';
  return;
}

$context = Timber::get_context();
$post = new TimberPost();
$context['post'] = $post;
$templates = array('page-'.$post->post_name.'.twig', 'page.twig');


// Load Products
$context['products'] = json_decode(file_get_contents( get_template_directory() . '/assets/dist/data/products.json'));
$context['comp_products'] = json_decode(file_get_contents( get_template_directory() . '/assets/dist/data/comp_products.json'));
// echo '<xmp>';print_r($context['products']);die;
$context['footer_links'] = array();
$x=0;
foreach( $context['products'] as $prod ) {
  if( in_array('heat-transfer', $prod->duratherm_fluid_type) ) {
    $context['footer_links'][$x]['name'] = $prod->name;
    $context['footer_links'][$x]['slug'] = $prod->slug;
    $x++;
  }
}
sort($context['footer_links']);
// echo '<xmp>'; print_r($context['footer_links']);die;


if (is_front_page()) {
  array_unshift($templates, 'front.twig');
  $args1 = array(
    'post_type' => 'testimonials',
    'posts_per_page' => -1,
    'meta_query' => array(
      'relation' => 'OR',
      array (
        'key' => 'sites',
        'value' => 'mainsite',
        'compare' => '='
      ),
      array (
        'key' => 'sites',
        'value' => 'both',
        'compare' => '='
      )
    )
  );
  $testimonials = Timber::get_posts( $args1 );
  $context['testimonials'] = [];
  foreach( $testimonials as $testimonial ) {
    if( $testimonial->custom['front_page_callout'] != '' ) {
      $context['testimonialsFront'][] = $testimonial;
    }
  }
  // echo '<xmp>'; print_r($context['post']); die;
}

if( is_page('testimonials')) {
  $args = array(
    'post_type' => 'testimonials',
    'posts_per_page' => -1
  );
  $context['testimonials'] = Timber::get_posts( $args );
  // echo '<xmp>'; print_r($context['testimonials']); echo 'hi';die;
}

if( is_page('news') ){
  $args = array(
    'posts_per_page' => -1
  );
  $context['posts'] = Timber::get_posts($args);
  // echo '<xmp>';print_r($context['posts']);die;
}
// echo '<xmp>';print_r($context);die;
//Load Microsite links
$args = array('name' => 'microsites');
$context['microsites'] = Timber::get_posts( $args );

if( is_page('industry') ) {
  $args = array (
    'post_type' => 'page',
    'meta_key' => '_wp_page_template',
    'meta_value' => 'byindustry-template.php',
    'order' => 'ASC',
    'posts_per_page' => -1
  );
  $posts = Timber::get_posts( $args );
  $context['posts'] = $posts;
}



Timber::render($templates, $context);
