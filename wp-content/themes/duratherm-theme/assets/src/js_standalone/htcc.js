function CoefficientCalculator(options) {
console.log(options)
    this.elId = options.el; 
    this.el = $('#'+options.el);
    this.products = options.products;
    
    if (typeof options.selectedProductId!='undefined' && options.selectedProductId === parseInt(options.selectedProductId)) {
        this.selectedProductId = options.selectedProductId;
        this.selectedProduct = this.getProduct(this.selectedProductId);
        $('#fluids-htcc').val(this.selectedProductId);
    } else {
        this.selectedProductId = null;
        this.selectedProduct = null
    }
    
    this.MSG_NFT = 'Not Fully Turbulent';
    
    // pare down product list to just Duratherm products
    var arr = [];
    for(var i=0; i<this.products.length; i++) {
        if (this.products[i].name.indexOf('Duratherm') !== -1 ) arr.push(this.products[i]);
    }
    this.products = arr;

    this.resolveProductProperties();
	
    
    
    // set the measurement system
    this.metric = options.metric;
    
    
    
    this.temperature        = new ConvertibleNumber('', this.metric, 'temperature', $('#temperature-htcc'), this);
    this.pipeDiameter       = new ConvertibleNumber('', this.metric, 'length', $('#pipe-diameter-htcc'), this);
    this.fluidVelocityMin   = new ConvertibleNumber('', this.metric, 'velocity', $('#fluid-velocity-min-htcc'), this);
    this.fluidVelocityMax   = new ConvertibleNumber('', this.metric, 'velocity', $('#fluid-velocity-max-htcc'), this);
    this.fluidVelocityInc   = new ConvertibleNumber('', this.metric, 'velocity', $('#fluid-velocity-inc-htcc'), this);
    this.specificHeat       = new ConvertibleNumber('', this.metric, 'specific heat', $('#fluid-specific-heat-htcc'), this);
    this.conductivity       = new ConvertibleNumber('', this.metric, 'conductivity', $('#fluid-conductivity-htcc'), this);
    this.density            = new ConvertibleNumber('', this.metric, 'density', $('#fluid-density-htcc'), this);
    
    this.attachEvents();
    
    this.switchMeasurementSystem();
    if (this.metric) {
    	$('#btn-metric').click();
        console.log(this.metric)
    } else {
        $('#btnStandard').click(); 
        console.log(this.metric)
    }

    $('#btnMetric').on('click', function() {
        this.metric = true
        console.log(this.metric)
    })
    $('#btnStandard').on('click', function() {
        this.metric = false
        console.log(this.metric)
    })
    

        
}
CoefficientCalculator.prototype.populateProducts = function() {
	var html = '<option value="-1">Select a Fluid</option>';
	for(var i=0; i< this.products.length; i++) {
		// console.log(this.products[i].name);
		html += '<option value="'+this.products[i].id+'">'+this.products[i].name+'</option>';
	}
	$('#populateProducts').append(html);
}
CoefficientCalculator.prototype.resolveProductProperties = function() {
	
	// loop through the products and create a temperature value for each degree
    for(var i=0; i<this.products.length; i++) {
        
        var temperatureValues = [];
        
        for(var j=0; j<this.products[i].fluid_data_standard.length-1; j++) {
            
            var current = this.products[i].fluid_data_standard[j];
            var next = this.products[i].fluid_data_standard[j+1];
            //// console.log(current,next);
            for(var k=parseInt(current.temperature); k<parseInt(next.temperature); k++) {
                
                var pct = (k-parseInt(current.temperature))/(parseInt(next.temperature) - parseInt(current.temperature));
                //// console.log(k,pct,parseFloat(current.density)+((parseFloat(next.density)-parseFloat(current.density))*pct));
                temperatureValues.push({
                    temperature:k,
                    density:                parseFloat(current.density)+((parseFloat(next.density)-parseFloat(current.density))*pct),
                    kinematic_viscosity:    parseFloat(current.kinematic_viscosity)+((parseFloat(next.kinematic_viscosity)-parseFloat(current.kinematic_viscosity))*pct),
                    dynamic_viscosity:      parseFloat(current.dynamic_viscosity)+((parseFloat(next.dynamic_viscosity)-parseFloat(current.dynamic_viscosity))*pct),
                    thermal_conductivity:   parseFloat(current.thermal_conductivity)+((parseFloat(next.thermal_conductivity)-parseFloat(current.thermal_conductivity))*pct),
                    heat_capacity:          parseFloat(current.heat_capacity)+((parseFloat(next.heat_capacity)-parseFloat(current.heat_capacity))*pct),
                    vapour_pressure:        parseFloat(current.vapour_pressure)+((parseFloat(next.vapour_pressure)-parseFloat(current.vapour_pressure))*pct)
                });
            }
            
            
        }
        temperatureValues.push(this.products[i].fluid_data_standard[j]);
        
        this.products[i].fluid_data_standard = temperatureValues;
        
    }
        // console.log(this.products);

}
CoefficientCalculator.prototype.getProduct = function(id) {
    if (id==null) return null;
    for(var i=0; i<this.products.length; i++) {
        if (parseInt(this.products[i].id)==parseInt(id)) return this.products[i];
    }
    return null;
}
CoefficientCalculator.prototype.getProductTempRange = function() {
    var min = parseInt(this.selectedProduct.fluid_data_standard[0].temperature);
    var max = parseInt(this.selectedProduct.fluid_data_standard[this.selectedProduct.fluid_data_standard.length-1].temperature);
    
    return {min:min, max:max, value:Math.floor((max-min)/2)};
}
CoefficientCalculator.prototype.getProductPropertiesAtTemp = function(product, temperature) {
    
    // round the temp to the closest integer
    temperature = Math.round(temperature);
    
	// find the temperature readings above and below the target temperature
	for(var i=0; i<product.fluid_data_standard.length; i++) {
		if (product.fluid_data_standard[i].temperature==temperature) {
			return product.fluid_data_standard[i];
		}
	}
    
    return false;
};
CoefficientCalculator.prototype.C2F = function(t) {
    if (t=='') return '';
	return parseInt(((parseFloat(t) * 9 / 5) + 32));
}
CoefficientCalculator.prototype.F2C = function(t) {
    if (t=='') return '';
	return parseInt(((parseFloat(t) - 32) * 5 / 9));
}



CoefficientCalculator.prototype.resolveProperties = function() {
	// console.log('rp');
	var selectedTemp = this.temperature.out(),
        minTemp = parseInt(this.selectedProduct.fluid_data_standard[0].temperature),
        maxTemp = parseInt(this.selectedProduct.fluid_data_standard[this.selectedProduct.fluid_data_standard.length-1].temperature),
        strSystem = (this.metric?'C':'F');
        
    if (this.metric) {
	    minTemp = this.F2C(minTemp);
	    maxTemp = this.F2C(maxTemp);
    }
    
    // check that temperature is within range of selected product
    if (selectedTemp > maxTemp) {
	    //// console.log(this.temperature.out() + strSystem + ' is too high for the selected product. Setting temperature to ' + maxTemp + strSystem);
        alert(this.temperature.out() + strSystem + ' is too high for the selected product. Setting temperature to ' + maxTemp + strSystem);
        this.temperature.set(maxTemp, this.metric);
        
    } else if (selectedTemp < minTemp) {
	    //// console.log(this.temperature.out() + strSystem + ' is too low for the selected product. Setting temperature to ' + minTemp + strSystem);
        alert(this.temperature.out() + strSystem + ' is too low for the selected product. Setting temperature to ' + minTemp + strSystem);
        this.temperature.set(minTemp, this.metric);
        
    }
    
	// set the product properties at the selected temperature
    var propsAtTemp = this.getProductPropertiesAtTemp(this.selectedProduct, this.temperature.toStandard());
    // console.log(selectedTemp,minTemp,maxTemp,strSystem,propsAtTemp);
    $('#fluid-viscosity-htcc').val(parseFloat(propsAtTemp.dynamic_viscosity).toFixed(2));
    this.specificHeat.set(parseFloat(propsAtTemp.heat_capacity), false);
    this.conductivity.set(parseFloat(propsAtTemp.thermal_conductivity), false);
    this.density.set(parseFloat(propsAtTemp.density), false);
    
};
CoefficientCalculator.prototype.resetFluidSelect = function() {
	$('#fluids-htcc').val(-1);
};
CoefficientCalculator.prototype.clearOutput = function() {
	$('#prandtl').text('');
	$('#output-htcc').empty();
}

CoefficientCalculator.prototype.attachEvents = function() {
    // console.log("events")
    var that = this;
    
    // measurement system change
    $('#btnMetric').click(function(event) {
        if (!that.metric) {
            that.metric = true;
            that.switchMeasurementSystem();
        }
    });
    $('#btnStandard').click(function(event) {
        if (that.metric) {
            that.metric = false;
            that.switchMeasurementSystem();
        }
    });
    
    // tubing/pipe radio changed
    $('input[name="pipe-or-tubing"]').change(function() {
	    //// console.log($(this).val());
    });
    
	// change the selected Duratherm product
    $('#fluids-htcc').change(function() {
	    
	    if ($(this).val()==that.selectedProductId || $(this).val()==-1) {
            //// console.log('no change in selected fluid', $(this).val());
            return false;
        }
        
	    // set the product
        that.selectedProductId = $(this).val();
        that.selectedProduct = that.getProduct(that.selectedProductId);
        
        if (that.temperature.isSet()) {
			that.resolveProperties();
		}
		that.clearOutput();
    });
    
    // temperature change
    $('#temperature-htcc').change(function(){
	    if ($('#fluids-htcc').val()!='-1') {
            // console.log("temp changed")
		    that.resolveProperties();
	    }
	    that.clearOutput();
    });
    
    // change any fluid properties
    $('#fluid-viscosity-htcc, #fluid-specific-heat-htcc, #fluid-conductivity-htcc, #fluid-density-htcc').change(function(){
	    that.resetFluidSelect();
	    that.clearOutput();
    });
    
    
    
    // change anything else
    $('#pipe-diameter-htcc, #fluid-velocity-min-htcc, #fluid-velocity-max-htcc, #fluid-velocity-inc-htcc, input:radio[name="roughness-htcc"]').change(function(){
	    that.clearOutput();
    });
    
    // enter my own data option
    $('#enter-own-data').click(function(){
	    
    });
    
    
    // reset
    $('a.reset-htcc').click(function(){
        var confirmReset = confirm("You are about to reset all the fields in this form, continue?");
        if( confirmReset == true ) {
    	    $('#frmHTCC input[type="text"]').val('');
    	    
    	    that.resetFluidSelect();
    	    $('input:radio[name="temp-system"][value="F"]').prop('checked', true);
    	    $('input:radio[name="roughness-htcc"][value="0.00015"]').prop('checked', true);
    	    that.clearOutput();
    	    
    	    that.selectedProductId = null;
    	    that.selectedProduct = null;
            return false;
	    } else {
            return false;
        }
    });
    
	// print
    $(".print").on("click", function(e) {
        var printFluid = $("#fluids-htcc").val() != -1 ? $("#fluids-htcc option:selected").text() : "No fluid selected";
        
        var printHeader = "<h2>Heat Transfer Calculator</h2>";
        printHeader += "<p>https://durathermfluids.com</p>";
        printHeader += "<h3>Input Data</h3>"
        printHeader += "<table>";
        printHeader += "<tr><td><strong>Temperature:</strong></td><td colspan='4'>" + $("#temperature-htcc").val() + $(".units-temp").html() + "</td></tr>";
        printHeader += "<tr><td><strong>Pipe Diameter:</strong></td><td colspan='4'>" + $("#temperature-htcc").val() + $(".units-diameter").html().replace("<br>"," ") + "</td></tr>";
        printHeader += "<tr><td><strong>Fluid Velocity:</strong></td>";
        printHeader += "<td><strong>" + $(".units-fv-min").html().replace("<br>"," ") + "</strong></td>";
        printHeader += "<td><strong>" + $(".units-fv-max").html().replace("<br>"," ") + "</strong></td>";
        printHeader += "<td colspan='2'><strong>" + $(".units-fv-inc").html().replace("<br>"," ") + "</strong></td></tr>";
        printHeader += "<tr><td></td><td>" + $("#fluid-velocity-min-htcc").val() + "</td>";
        printHeader += "<td>" + $("#fluid-velocity-max-htcc").val() + "</td>";
        printHeader += "<td colspan='2'>" + $("#fluid-velocity-inc-htcc").val() + "</td>";
        printHeader += "</tr>";
        printHeader += "<tr><td><strong>Fluid:</strong></td><td>" + printFluid + "<td></tr>";
        printHeader += "<tr><td><strong>Fluid Properties:</strong></td>";
        printHeader += "<td><strong>" + $(".units-viscosity").html().replace("<br>"," ") + "</strong></td>";
        printHeader += "<td><strong>" + $(".units-specific-heat").html().replace("<br>"," ") + "</strong></td>";
        printHeader += "<td><strong>" + $(".units-conductivity").html().replace("<br>"," ") + "</strong></td>";
        printHeader += "<td><strong>" + $(".units-density").html().replace("<br>"," ") + "</strong></td></tr>";
        printHeader += "<tr><td></td><td>" + $("#fluid-viscosity-htcc").val() + "</td>";
        printHeader += "<td>" + $("#fluid-specific-heat-htcc").val() + "</td>";
        printHeader += "<td>" + $("#fluid-conductivity-htcc").val() + "</td>";
        printHeader += "<td>" + $("#fluid-density-htcc").val() + "</td>";
        printHeader += "</tr>";
        printHeader += "</table>";
        printHeader += "<h3>Results</h3>";

        var css = '<style>';
        css += "body { font-family: sans-serif; -webkit-print-color-adjust: exact; }";
        css += '.page-header { display: none; }';
        css += 'table { width: 100%; border-spacing: 0; border-collapse: collapse; font-size: 0.9em;}';
        css += "td { padding: 10px 5px; }";
        css += "th { padding: 10px 5px;}";
        css += "tr { border-bottom:1px solid #ccc;}";
        css += "#outputTable tr:nth-child(even) { background-color: #ddd }";
        css += "#outputTable { text-align: center; }";
        css += '</style>';

        var prtContent = document.getElementById("output-htcc");
        var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0,titlebar=0');
        WinPrint.document.write("<title>Duratherm Heat Transfer Coefficient Calculator</title>");
        WinPrint.document.write(css);
        WinPrint.document.write(printHeader);
        WinPrint.document.write(prtContent.innerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
        WinPrint.close(); 
    })
    
    // download
    $('a.export').click(function(){
        ExportCSV.apply(this);
    });

}


CoefficientCalculator.prototype.isNumeric = function(a){
    return!Array.isArray(a)&&a-parseFloat(a)>=0;
}
CoefficientCalculator.prototype.isInt = function(data) {
    return (data === parseInt(data));
}
CoefficientCalculator.prototype.isFloat = function(data) {
    // console.log(typeof data, data,parseFloat(data),data === parseFloat(data),data === parseFloat(data));
    return (data === parseFloat(data));
}
CoefficientCalculator.prototype.generateOutput = function() {
    
    var html = '';
    var inputs = {
        fvMinimum: $('#fvMinimum')
    };
    
    var temp			= this.temperature.toStandard();
    var viscosity		= $("#fluid-viscosity-htcc").val() * 2.42;
    var heat			= this.specificHeat.toStandard();
    var conductivity	= this.conductivity.toStandard();
    var density			= this.density.toStandard();;
    var roughness		= $("#roughness-tubing").is(":checked") ? 0.00005 : 0.00015;
    var pipeDiameter	= this.pipeDiameter.toStandard() / 12;
    var prandtl			= viscosity * heat / conductivity;

    if (!this.metric) {
        html += '<table id="outputTable"><tr><th>Velocity (ft/s)</th><th>Nusselt Number</th><th>Reynolds Number</th><th>Pressure Drop (psi/100 ft)</th><th>Heat Transfer Coefficient (BTU/ft&sup2;&middot;hr&middot;F&deg;)</th></tr>';
    } else {
        html += '<table id="outputTable"><tr><th>Velocity (m/s)</th><th>Nusselt Number</th><th>Reynolds Number</th><th>Pressure Drop (kPa/25m)</th><th>Heat Transfer Coefficient (W/m&sup2;&middot;K)</th></tr>';
    }

    var velocityMin = this.fluidVelocityMin.toStandard() * 3600
    var velocityMax = this.fluidVelocityMax.toStandard() * 3600;
    var velocityStep = this.fluidVelocityInc.toStandard() * 3600;
    
    for(var velocity=velocityMin; velocity<=velocityMax; velocity+=velocityStep) {
        
        var reynolds = density * velocity * pipeDiameter / viscosity;
        var nusselt = 0.025 * Math.pow(reynolds,0.79) * Math.pow(prandtl,0.42) * 1.023;
        var httc = (reynolds > 10000) ? (nusselt * conductivity) / pipeDiameter : this.MSG_NFT;  
        // console.log(density, velocity, pipeDiameter)
        // calculate the pressure drop
        var ff = new Array();
        var start = 0.03;
        for( var i=0; i<=7; i++ ) {
            ff[i] = new Array();
            ff[i][0] = start;
            ff[i][1] = -0.86 * Math.log( ( roughness / (3.7 * pipeDiameter ) ) + ( 2.51 / ( reynolds * Math.pow(ff[i][0],0.5) ) ) );
            ff[i][2] = start = 1/Math.pow(ff[i][1],2);
        }
        var frictionFactor = ( Math.abs( 1 - ( ff[7][2] / ff[7][0] ) ) < 0.05 ) ? ff[7][2] : "not coverged";
        var pdrop = 0.0001078 * frictionFactor * (100 / pipeDiameter) * density * Math.pow( (velocity / 3600), 2 );
        
        // if we are metric, convert pdrop and htcc
        if (this.metric) {
            if (httc!=this.MSG_NFT) httc *= 1.49 * 3.808364;
            pdrop *= 5.6551487;
        }
        
        var convertedVelocity = this.metric ? (velocity * 0.3048 / 3600) : (velocity / 3600);
        convertedVelocity = convertedVelocity.toFixed(1);  
     // // console.log(roughness); return;
        
        html += '<tr><td>'
        	+convertedVelocity
        	+'</td><td>'
        	+nusselt.toFixed(0)
        	+'</td><td>'
        	+reynolds.toFixed(0)
        	+'</td><td>'
        	+pdrop.toFixed(0)
        	+'</td><td>'
        	+(httc==this.MSG_NFT ? this.MSG_NFT : httc.toFixed(0))
        	+'</td></tr>';
        
    }
    
    
    
    html += '</table>';
    html += '<p class="small" style="margin-top: 1em">This calculator is provided for estimation purposes only and is not intended to be used for engineering without verification. Duratherm has made every attempt to ensure the accuracy and reliability of the information provided on this website. However, the information is provided "as is" without warranty of any kind.</p>';
    
    $('#output-htcc').empty().html(html);
    
    $('#prandtl').text('Prandtl Number: ' + prandtl.toFixed(2));
    
    // animate to the table
    $('html, body').animate({
        scrollTop: $('#pipe-diameter-htcc').offset().top
    }, 1000);
    
}


function ExportCSV() {
	console.log('csv')
	
	var $rows = $('#pct table tr'),
    
        // actual delimiter characters for CSV format
        colDelim = '","',
        rowDelim = '"\r\n"';
	
		// construct a 2d array holding all text values we want to print out
		var numRows = 40, numCols = 5, data = createArray(numRows, numCols);
		data[0][0] = "Heat Transfer Fluid";
		data[1][0] = "https://durathermfluids.com/";
		data[2][0] = "This site is owned and maintained by Duratherm Extended Life Fluids " + new Date().getFullYear() +". This calculator is provided for estimation purposes only and is not intended to be used for engineering without verification. Duratherm has made every attempt to ensure the accuracy and reliability of the information provided on this website. ";
		data[3][0] = "However, the information is provided 'as is' without warranty of any kind.";
		data[4][0] = "";
		data[5][0] = "";
		
		data[6][0] = "Fluid:";
		data[6][1] = $('#fluids-htcc option:selected').text();
		if( $('#btnStandard').hasClass('active') ) {
            data[7][0] = "Temperature (F):";
        } else {
            data[7][0] = "Temperature (C):";
        }
		
        data[7][1] = $('#temperature-htcc').val();
        
		if( $('#btnStandard').hasClass('active') ) {
            data[8][0] = "Pipe Diameter (inches):";
        } else {
            data[8][0] = "Pipe Diameter (mm):";
        }
		
		
		data[8][1] = $('#pipe-diameter-htcc').val();
        
		if( $('#btnStandard').hasClass('active') ) {
            data[9][0] = "Fluid Velocity (ft./s) Minimum:";
        } else {
            data[9][0] = "Fluid Velocity (m/s) Minimum:";
        }
		data[9][1] = $('#fluid-velocity-min-htcc').val();
		
		if( $('#btnStandard').hasClass('active') ) {
            data[10][0] = "Fluid Velocity (ft./s) Minimum:";
        } else {
            data[10][0] = "Fluid Velocity (m/s) Minimum:";
        }
		data[10][1] = $('#fluid-velocity-max-htcc').val();
		
		if( $('#btnStandard').hasClass('active') ) {
            data[11][0] = "Fluid Velocity (ft./s) Minimum:";
        } else {
            data[11][0] = "Fluid Velocity (m/s) Minimum:";
        }
		data[11][1] = $('#fluid-velocity-inc-htcc').val();
		
		data[12][0] = "Viscosity (cP):";
		data[12][1] = $('#fluid-viscosity-htcc').val();
		
        
        if( $('#btnStandard').hasClass('active') ) {
            console.log('standard')
            data[13][0] = "Specific Heat (btu/lb. ft. F):";
        } else {
            console.log('metric')
            data[13][0] = "Specific Heat (kJ/kg·K):";
        }
		data[13][1] = $('#fluid-specific-heat-htcc').val();

        
        if( $('#btnStandard').hasClass('active') ) {
            data[14][0] = "Thermal Conductivity (btu/hr. ft. F):";
        } else {
            data[14][0] = "Thermal Conductivity (W/m·K):";
        }
		data[14][1] = $('#fluid-conductivity-htcc').val();

        
        if( $('#btnStandard').hasClass('active') ) {
            data[15][0] = "Density (lb/ft3):";
        } else {
            data[15][0] = "Density (kg/L):";
        }
		data[15][1] = $('#fluid-density-htcc').val();

		data[16][0] = $('#prandtl').html();
		data[16][1] = ""

		data[17][0] = "";
		data[17][1] = "";
		
		
		// table headers
		var rowNum = 18, i = 0;
		$.each($("#outputTable tr"), function(index, el) {
			data[rowNum] = [];
			$.each($(el).find('th, td'), function(indexChild, elChild) {
				// console.log(rowNum, i, data[rowNum]);
				data[rowNum][i] = $(elChild).text();
				i++;
				if (i==5) i=0;
			});
			rowNum++;
		});
		
		
		var csvContent = "";
        dataString = "";
		data.forEach(function(infoArray, index){
		   dataString = infoArray.join(",");
		   csvContent += index < data.length ? dataString+ "\n" : dataString;
		}); 
		
		var textEncoder = new CustomTextEncoder('windows-1252', {NONSTANDARD_allowLegacyEncoding: true}),
			csvContentEncoded = textEncoder.encode([csvContent]),
			blob = new Blob([csvContentEncoded], {type: 'text/csv;charset=windows-1252;'});
			
		saveAs(blob, 'httc.csv');
}
function createArray(length) {
    var arr = new Array(length || 0),
        i = length;

    if (arguments.length > 1) {
        var args = Array.prototype.slice.call(arguments, 1);
        while(i--) arr[length-1 - i] = createArray.apply(this, args);
    }

    return arr;
}


CoefficientCalculator.prototype.switchMeasurementSystem = function() {
    if (this.metric) {
        $('#btnStandard').removeClass('btn-active');
        $('#btnMetric').addClass('btn-active');
        
        $('.units-temp').html('&deg;C');
        $('.units-diameter').html('mm<br>(inside dia.)');
        $('.units-fv-min').html('Min.<br>(m/s)');
        $('.units-fv-max').html('Max.<br>(m/s)');
        $('.units-fv-inc').html('Increment<br>(m/s)');
        $('.units-specific-heat').html('Specific Heat<br>kJ/kg&middot;K');
        $('.units-conductivity').html('Conductivity<br>W/m&middot;K');
        $('.units-density').html('Density<br>kg/L');
        
    } else {
        $('#btnStandard').addClass('btn-active');
        $('#btnMetric').removeClass('btn-active');
        
        $('.units-temp').html('&deg;F');
        $('.units-diameter').html('Inches<br>(inside dia.)');
        $('.units-fv-min').html('Min.<br>(ft/s)');
        $('.units-fv-max').html('Max.<br>(ft/s)');
        $('.units-fv-inc').html('Increment<br>(ft/s)');
        $('.units-specific-heat').html('Specific Heat<br>BTU/lb&middot;F&deg;');
        $('.units-conductivity').html('Conductivity<br>BTU/hr&middot;ft&middot;F&deg;');
        $('.units-density').html('Density<br>lb/ft<sup>3</sup>');
        
    }
    
    //this.temperature.refresh();
    this.specificHeat.refresh();
    //this.pipeDiameter.refresh();
    //this.fluidVelocityMin.refresh();
    //this.fluidVelocityMax.refresh();
    //this.fluidVelocityInc.refresh();
    this.conductivity.refresh();
    this.density.refresh();
    
    this.clearOutput();
}

function ConvertibleNumber(num, isMetric, type, el, htcc) {
    this.num = num;
    this.isMetric = isMetric;
    this.type = type;
    this.el = el;
    this.htcc = htcc;
    this.decimalPlaces = 2;
    this.attachEvents();
}
ConvertibleNumber.prototype.refresh = function() {
    $(this.el).val(this.htcc.metric ? this.toMetric() : this.toStandard());
}
ConvertibleNumber.prototype.attachEvents = function() {
    var that = this;
    //// console.log('attaching onChange event to: #' + $(that.el).attr('id'));
    $(that.el).change(function(){
        that.num = parseFloat($(this).val());
        that.isMetric = that.htcc.metric;
        //// console.log($(that.el).attr('id') + ' onChange');
    });
}
ConvertibleNumber.prototype.isSet = function() {
    return this.num!=='';
}
ConvertibleNumber.prototype.set = function(num, isMetric) {
    this.num = num;
    this.isMetric = isMetric;
    this.refresh();
}
ConvertibleNumber.prototype.out = function() {
    return this.isMetric ? this.toMetric() : this.toStandard();
}
ConvertibleNumber.prototype.toStandard = function() {
    
    if (!this.isMetric || this.num=='') {
        return this.num;
    }
    
	if (this.type=='temperature') {
        return ((parseFloat(this.num) * 9 / 5) + 32).toFixed(this.decimalPlaces);
        
    } else if (this.type=='length') {
        return (parseFloat(this.num) / 25.4).toFixed(this.decimalPlaces);
        
    } else if (this.type=='velocity') {
        return (parseFloat(this.num) / 0.3048).toFixed(this.decimalPlaces);
        
    } else if (this.type=='specific heat') {
        return (parseFloat(this.num) / 4.1868).toFixed(this.decimalPlaces);
        
    } else if (this.type=='conductivity') {
        return (parseFloat(this.num) / 1.73073).toFixed(this.decimalPlaces);
        
    } else if (this.type=='density') {
        return (parseFloat(this.num) / 0.016018).toFixed(this.decimalPlaces);
        
    }
}
ConvertibleNumber.prototype.toMetric = function() {
	
    if (this.isMetric || this.num=='') {
        return this.num;
    }
    
	if (this.type=='temperature') {
        return ((parseFloat(this.num) - 32) * 5 / 9).toFixed(this.decimalPlaces);
        
    } else if (this.type=='length') {
        return (parseFloat(this.num) * 25.4).toFixed(this.decimalPlaces);
        
    } else if (this.type=='velocity') {
        return (parseFloat(this.num) * 0.3048).toFixed(this.decimalPlaces);
        
    } else if (this.type=='specific heat') {
        return (parseFloat(this.num) * 4.1868).toFixed(this.decimalPlaces);
        
    } else if (this.type=='conductivity') {
        return (parseFloat(this.num) * 1.73073).toFixed(this.decimalPlaces);
        
    } else if (this.type=='density') {
        return (parseFloat(this.num) * 0.016018).toFixed(this.decimalPlaces);
        
    }
}
