$('#glossary-search').keyup(function(e) {
  var testPattern = $(this).val()
  if( testPattern.length >= 3 ) {
    $(".term-filter").removeClass('active')
    $('.terms').each(function() {
      $toSearch = $( this ).find('p').text()
      if( $toSearch.toLowerCase().indexOf(testPattern.toLowerCase()) === -1 ) {
        $(this).hide()
      } else {
        $(this).show()
      }
    })
  } else {
    $('.terms').show()
  }
})

$(".term-filter").on("click", function() {
  $(".term-filter").removeClass('active')
  $(this).addClass('active')
  var expression
  var filter = $(this).data("filter")
  if(filter == 'all') {
    $(".terms").show()
    return
  }
  var exp = '^['+filter+filter.toUpperCase()+']*$'
  $(".terms").fadeOut()
  expression = new RegExp(exp)
  $(".term").each(function( index ) {
    var start = $(this).text().substring(0,1)
    if(expression.test(start)) {
      var match = $(this).text()
      $(".terms").each(function() {
        if( $(this).data('term')==match) {
          $(this).fadeIn()
        } 
      })
    }
  })
})