tempSelector()

	function tempSelector() {
		var selector = getParameterByName('selector')
		// console.log('selector')
		if( localStorage.getItem("tempValue") !== null ){
			var tempValue = localStorage.getItem("tempValue")
			if(tempValue === 'c') {
				var localTemp = 'c'
				convertTemp('c')
			} else {
				var localTemp = 'f'
				convertTemp('f')
			}
		} else {
			localStorage.setItem('tempValue', 'c')
			var localTemp = 'c'
			convertTemp('c')
		}
	}

	$('.c').on('click', function() {
		localStorage.setItem('tempValue', 'c')
		convertTemp('c')
	})

	$('.f').on('click', function() {
		localStorage.setItem('tempValue', 'f')
		convertTemp('f')
	})

	function convertTemp( $value ) {
		if( $value === 'f' ) {
			$('.metric').hide()
			$('.standard').show()
			$('.temp-value').html('F')
			$('.c').removeClass('active')
			$('.f').addClass('active')
		} else if( $value === 'c' ) {
			$('.standard').hide()
			$('.metric').show()
			$('.temp-value').html('C')
			$('.c').addClass('active')
			$('.f').removeClass('active')
		}
	}
  // Add a product row in the contact form
  var template = $("#product-row-template")
  var formCount = 1;
  var hasQuote = false;
  $("#add-product").on("click", function() {
    var removeMe = "<div class='col-sm-1'><a href='#' class='remove-me'><i class='fa fa-minus-circle' aria-hidden='true'></i></a></div>";
    formCount++;
    var lastGroup = $(".product-row").last();
  
    template.clone()
      .attr('data-rowid', formCount)
      .removeClass('d-none')
      .addClass('product-row-form')
      .attr('id', 'product-row' + formCount)
      .find(':input').each(function(){
      var newId = this.id + formCount;
      $(this).prev().attr('for', newId);
      this.id = newId;
      this.classList.add('control')
      this.name = this.name + formCount
    }).end()
    .insertAfter(lastGroup).append(removeMe).hide().slideDown(300);
    return false;
  });
  
  
  $(document).on("click", ".remove-me", function(e) {
    e.preventDefault();
    $(this).closest(".product-row").slideUp(500, function() {
      $(this).remove()
    });
  })
  // End add a product
  
  //Contact Form selectors//
  $('.c1').on('click', function(e) {
    e.preventDefault()
    $(this).addClass('active')
    $(this).parent().find('.f1').removeClass('active')
    if($(this).hasClass('temp-select')) {
      $('#temp-type').val('C')
    } else {
      $('#vol-type').val('liters')
    }
  })
  $('.f1').on('click', function(e) {
    e.preventDefault()
    $(this).addClass('active')
    $(this).parent().find('.c1').removeClass('active')
    if($(this).hasClass('temp-select')) {
      $('#temp-type').val('F')
    } else {
      $('#vol-type').val('gallons')
    }
  })
  
  $(".toggle-display").on("click", function(e) {
    e.preventDefault()
    $(this).next(".toggle-div").slideToggle();
    if($(this).find("i").hasClass("fa-chevron-down")) {
      $(this).find("i").removeClass("fa-chevron-down").addClass("fa-chevron-up")
      hasQuote = true;
    } else {
      $(this).find("i").removeClass("fa-chevron-up").addClass("fa-chevron-down")
      hasQuote = false;
    }
  })
  $(document).ready(function() {
    if( getParameterByName('spec') === 'quote') {
      $(".toggle-div").slideToggle();
      $(this).find("i").removeClass("fa-chevron-down").addClass("fa-chevron-up")
      hasQuote = true;
    }
    if( typeof page != 'undefined' && page === 'contact' ) {
      var currentProduct = getParameterByName('product')
      var currentType = getParameterByName('type')
      if( currentProduct !== null ) {
       $(".toggle-div").slideToggle();
       hasQuote = true;
       $(this).find("i").removeClass("fa-chevron-down").addClass("fa-chevron-up")
       $('#quote-type1').val(currentType.replace('_',' '))
       var currentForm = $('#quote-type1').parent().parent().parent().data('rowid')
       var productType = currentType.replace('_',' ')
       updateProductForm( currentForm, productType, currentProduct);
      }

      $(document).on('change', '.product-select', function(){
        var currentForm = $(this).parent().parent().parent().data('rowid')
        var productType = $(this).val()
        updateProductForm( currentForm, productType)
      })
    }

    $('#contact-form').submit(function(e) {
      e.preventDefault()
      $('.loader').css('display','inline-block')
      $('#contact-submit').prop('disabled', true)
      var html = 'About You\r\n \r\n';
      var temp1 = 'C'
      var units = 'litres'
      if($('.f').hasClass('active')) {
        temp1 = 'F'
        units = 'gallons'
      }

      html += 'First Name: ' + $('#first_name').val() + '\r\n'
      html += 'Last Name: ' + $('#last_name').val() + '\r\n'
      html += 'Company: ' + $('#company').val() + '\r\n'
      html += 'Email: ' + $('#email').val() + '\r\n'
      html += 'Country: ' + $('#country').val() + '\r\n'
      html += 'Phone Number: ' + $('#phone').val() + '\r\n'
      html += 'Comments: ' + $('#about-comments').val() + '\r\n \r\n'
      if( $('#temperature').val() === '' && $('#application').val() === '' && $('#volume-contact').val() === '' ) {

      } else {
        html+= 'About Application\r\n \r\n'
        html += 'Temperature: ' + $('#temperature').val() + ' ' + temp1 + '\r\n'
        html += 'Volume: ' + $('#volume-contact').val() + ' ' + units + '\r\n'
        html += 'Industry Details: ' + $('#application').val() + '\r\n \r\n'
      }
      if(hasQuote) {
        html+= 'Quotes\r\n \r\n'
        var quotes = ''
        $('.product-row-form').each(function() {
          var quote = ''
          $('.control', this).each(function() {
            var curVal = $(this).val()
            if(curVal === 'undefined') {
              curVal = 'blank'
            }
            quote += $(this).data('field') + ': ' + curVal + '\r\n'
          })
          html += quote + '\r\n'
        })
      }
      // $('#email-value').html(html)

      var data = {
        action: 'mail_before_submit',
        toemail: $('#email').val(),
        withquote: hasQuote,
        email_body: html,
        _ajax_nonce: $('#my_email_ajax_nonce').data('nonce')
      }
      $.post(window.location.origin + "/wp-admin/admin-ajax.php", data, function(response) {
        if (response === 'email sent') {
          if (hasQuote) {
            window.location = '/contact/thankyou-rfq?email=' + $('#email').val()
          } else {
            window.location = '/contact/thankyou-contact?email=' + $('#email').val()
          }
        }
        console.log('Got this from the server: ' + response);
      });
    })
    
  })


function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function updateProductForm(currentForm, productType, currentProduct) {
  $('#quote-products' + currentForm)
  .find('option')
  .remove()
  .end()
  .append('<option value="0">Select a Product</option>')

  var thermalFluids = [
    'Duratherm 600',
    'Duratherm 630',
    'Duratherm G',
    'Duratherm LT',
    'Duratherm FG',
    'Duratherm S',
    'Duratherm 450',
    'Duratherm XLT-120',
    'Duratherm XLT-50',
    'Duratherm HTO',
    'Duratherm HTO-FG',
    'Duratherm HTO22-FG',
    'Duratherm HF',
    'Duratherm 450-FG',
    'Duratherm HF-FG',
  ]
  var openBath = [
    'Duratherm S',
    'Duratherm G',
    'Duratherm G-LV',
  ]
  var glycolCoolants = [
    'Intercool P-300 &mdash; 55%',
    'Intercool P-300 &mdash; 50%',
    'Intercool P-300 &mdash; 40%',
    'Intercool P-300 &mdash; 30%',
    'Intercool OP-100 &mdash; 50%',
    'Intercool OP-100 &mdash; 40%',
    'Intercool OP-100 &mdash; 30%'
  ]

  var systemCleaners = [
    'DuraClean',
    'U-Clean',
    'DuraClean Ultra',
    'DuraClean LSC'
  ]

  var array;
  switch (productType) {
    case 'Thermal Fluids':
      array = thermalFluids
      break;
    case 'Glycol Coolants':
      array = glycolCoolants
      break;
    case 'System Cleaners':
      array = systemCleaners
      break;
    case 'Open Bath':
      array = openBath
      break;
  }
  array.map(function(value) {
    var option = value
    $('#quote-products' + currentForm)
    .prop('disabled', '')
    .append('<option value="' + option +'">' + value + '</option>')
  })
  
  if( typeof currentProduct !== 'undefined') {
    var prodVal = currentProduct
    $('#quote-products1').val(prodVal).trigger('change')
  }
}
