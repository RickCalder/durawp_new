
if( window.location.pathname.indexOf('/resources/calculators/') > 0 ) {

	$('.visc2').keyup(function(){
		$(this).val($(this).val().replace(/(\.\d{2})\d+/g, '$1'))
	})
	$('.dens2').keyup(function(){
		$(this).val($(this).val().replace(/(\.\d{2})\d+/g, '$1'))
	})
	$('.cond3').keyup(function(){
		$(this).val($(this).val().replace(/(\.\d{3})\d+/g, '$1'))
	})
	$('.heat3').keyup(function(){
		$(this).val($(this).val().replace(/(\.\d{3})\d+/g, '$1'))
	})
}

$(".calculator-select").on("change", function(){
	var $target = $(this).val();
	if( $target === 'null' ) return false;
	window.location = '/resources/calculators/' + $target
})


function conductionHeatTransfer() {
	var r = ((fl('conductivityCH') * fl('areaCH') * (fl('temp1CH') - fl('temp2CH')) * fl('timeCH')) / fl('thicknessCH'));
	$('#result2CH').val(r.toFixed(2));
	return false;
}
function basicHeatTransfer() {
	var r = (fl('massSH') * fl('specific-heatSH') * (fl('temp1SH') - fl('temp2SH')));
	$('#resultSH').val(r.toFixed(2));
	return false;
}
function naturalHeatTransfer() {
	var r = fl('surface-areaNC') * fl('coefficientNC') * (fl('surface-tempNC') - fl('bulk-tempNC'));
	$('#result3NC').val(r.toFixed(2));
	return false;
}
function thermalExpansionViaDensities() {
	var coefficient = ((1/fl('density2TE')) - (1/fl('density1TE'))) / ((1/fl('density1TE')) * (fl('temp2TE')-fl('temp1TE'))) * 100;
	var pct_change = coefficient * (fl('temp2')-fl('temp1'));
	var volume_change = fl('volume1TE') * coefficient * (fl('temp2TE') - fl('temp1TE'));
	var volume2 = Number(fl('volume1TE')) + volume_change;
	$('#coefficient1TE').val(coefficient.toFixed(2));
	$('#pct-change').val((pct_change*100).toFixed(2) + '%');
	$('#volume-change').val(volume_change.toFixed(2));
	$('#volume2').val(volume2.toFixed(2));
	return false;
}
function operatingViscosity() {
	var cB = (log10(log10(fl('viscosity1OV')+0.7)) - log10(log10(fl('viscosity2OV')+0.7))) / (log10(273.15+fl('temp2OV'))-log10(273.15+fl('temp1OV')));
	var cA = (cB * log10(fl('temp1OV')+273.15)) + log10(log10(fl('viscosity1OV')+0.7));
	var viscosity = Math.pow(10, Math.pow(10, (cA - (cB * log10(273.15+fl('operational-tempOV'))))))-0.7;
	$('#resultOV').val(viscosity.toFixed(2));
	return false;
}
function fluidFlow1() {
	var r = fl('flow-rate1AF') / ((Math.PI/4) * (Math.pow(fl('pipe-diameter1AF'), 2)));
	$('#avg-flow-velocity1AF').val(r.toFixed(2));
	return false;
}
function fluidFlow2() {
	var r = fl('avg-flow-velocity2VF') * ((Math.PI/4) * (Math.pow(fl('pipe-diameter2VF'), 2)));
	$('#flow-rate2VF').val(r.toFixed(2));
	return false;
}
function volumeCylinder() {
	var r = Math.PI * (Math.pow((fl('diameterVC')/2), 2)) * fl('lengthVC');
	$('#resultCylinder').val(r.toFixed(2));
	return false;
}
function volumeSphere() {
	var r = (4/3) * Math.PI * (Math.pow((fl('diameterVS')/2), 3));
	$('#resultSphere').val(r.toFixed(2));
	return false;
}
function volumeRectangularPrism() {
	var r = fl('length1RP') * fl('length2RP') * fl('length3RP');
	$('#resultRectangularPrism').val(r.toFixed(2));
	return false;
}



function log10(val) {
	return Math.log(val) / Math.LN10;
}

function fl(elId) {
	return Number(parseFloat($('#'+elId).val()).toFixed(2));
}

// reset calc form
$('a.reset').click(function(){
	$(this).closest('form').find('input').val('');
	return false;
});


