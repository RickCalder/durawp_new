	$(document).ready(function() {
 	$('#BHTform').validate({
    rules: {
	    'massSH': {required: true, number: true},
	    'temp1SH': {required: true, number: true},
	    'temp2SH': {required: true, number: true},
	    'specific-heatSH': {required: true, number: true}
		},
    errorClass: "error",
    errorElement: "span",
    errorPlacement: function(error, element) {
			error.addClass('help-inline').insertAfter(element);
		},
    submitHandler: function(form) {
      basicHeatTransfer();
		}
  });

	$('#CHTform').validate({
    rules: {
	    'conductivityCH': {required: true, number: true},
	    'areaCH': {required: true, number: true},
	    'temp1CH': {required: true, number: true},
	    'temp2CH': {required: true, number: true},
	    'timeCH': {required: true, number: true},
	    'thicknessCH': {required: true, number: true}
		},
    errorClass: "error",
    errorElement: "span",
    errorPlacement: function(error, element) {
			error.addClass('help-inline').insertAfter(element);
		},
    submitHandler: function(form) {
      conductionHeatTransfer();
		}
  });

  $('#NFCForm').validate({
    rules: {
	    'surface-areaNC': {required: true, number: true},
	    'coefficientNC': {required: true, number: true},
	    'bulk-tempNC': {required: true, number: true},
	    'surface-tempNC': {required: true, number: true}
		},
    errorClass: "error",
    errorElement: "span",
    errorPlacement: function(error, element) {
			error.addClass('help-inline').insertAfter(element);
		},
    submitHandler: function(form) {
      naturalHeatTransfer();
		}
  });
});