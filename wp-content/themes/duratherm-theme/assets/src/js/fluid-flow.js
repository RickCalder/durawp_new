$('#fluidFlowForm1').validate({
  rules: {
    'flow-rate1AF': {required: true, number: true},
    'pipe-diameter1AF': {required: true, number: true}
  },
  errorClass: "error",
  errorElement: "span",
  errorPlacement: function(error, element) {
    error.addClass('help-inline').insertAfter(element);
  },
  submitHandler: function(form) {
    fluidFlow1();
  }
});
$('#fluidFlowForm2').validate({
  rules: {
    'avg-flow-velocity2VF': {required: true, number: true},
    'pipe-diameter2VF': {required: true, number: true}
  },
  errorClass: "error",
  errorElement: "span",
  errorPlacement: function(error, element) {
    error.addClass('help-inline').insertAfter(element);
  },
  submitHandler: function(form) {
    fluidFlow2();
  }
});