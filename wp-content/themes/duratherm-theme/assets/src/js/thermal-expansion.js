$('#thermalForm').validate({
	    rules: {
		    'temp1TE': {required: true, number: true},
		    'temp2TE': {required: true, number: true},
		    'density1TE': {required: true, number: true},
		    'density2TE': {required: true, number: true},
		    'volume1TE': {required: true, number: true}
		},
        errorClass: "error",
        errorElement: "span",
        errorPlacement: function(error, element) {
			error.addClass('help-inline').insertAfter(element);
		},
        submitHandler: function(form) {
	        thermalExpansionViaDensities();
		}
    });