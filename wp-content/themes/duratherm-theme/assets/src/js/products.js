"use strict";

$(document).ready(function(){

	if( localStorage.getItem("productsArray") !== null ){
    var productsArray = JSON.parse(localStorage.getItem("productsArray"));
	}
	if( typeof productsArray === 'undefined' ) return;
	// Temperature Selector
	tempSelector()

	function tempSelector() {
		var selector = getParameterByName('selector')
		// console.log('selector')
		if( localStorage.getItem("tempValue") !== null ){
			var tempValue = localStorage.getItem("tempValue")
			if(tempValue === 'c') {
				var localTemp = 'c'
				convertTemp('c')
			} else {
				var localTemp = 'f'
				convertTemp('f')
			}
		} else {
			localStorage.setItem('tempValue', 'c')
			var localTemp = 'c'
			convertTemp('c')
		}
	}

	$('.c').on('click', function() {
		localStorage.setItem('tempValue', 'c')
		convertTemp('c')
	})

	$('.f').on('click', function() {
		localStorage.setItem('tempValue', 'f')
		convertTemp('f')
	})

	function convertTemp( $value ) {
		if( $value === 'f' ) {
			$('.metric').hide()
			$('.standard').show()
			$('.temp-value').html('F')
			$('.c').removeClass('active')
			$('.f').addClass('active')
			displayProducts(productsArray)
		} else if( $value === 'c' ) {
			$('.standard').hide()
			$('.metric').show()
			$('.temp-value').html('C')
			$('.c').addClass('active')
			$('.f').removeClass('active')
			displayProducts(productsArray)
		}
	}


	//product selector change path
	$("#product-dropdown").change( function() {
		var nextProduct = $(this).find(":selected").val();
		location.href = nextProduct;
	})

	//check we're on the products page
	// if(typeof productsPage == 'undefined') return;
  // if( location.pathname !== productsPage) return;
  if(window.location.pathname !== '/heat-transfer-fluid/') return
	sort("high", "maxtemp");
	$("#sortby").val("maxtemp-high");
	if( localStorage.getItem("filterSettings") !== null ){
		var filterSettings = JSON.parse(localStorage.getItem("filterSettings"));
		filterSettings.max_temp ? $("#ff-max-temp-value").val(filterSettings.max_temp) : '';
		filterSettings.min_temp ? $("#ff-min-temp-value").val(filterSettings.min_temp) : '';
		filterSettings.flashpoint ? $("#min-flashpoint").val(filterSettings.flashpoint) : '';
		filterSettings.food_grade =="yes" ? $("#food-grade").prop("checked", true) : '';
		if(filterSettings.application) {
			$( "#" + filterSettings.application ).prop("checked", true);
			if( filterSettings.application == "need-heat-cool" ) {
				$("#ff-max-temp-value-container").show();
				$("#ff-min-temp-value-container").show();
			} else if ( filterSettings.application == "need-heat" ) {
				$("#ff-max-temp-value-container").show();
			} else if ( filterSettings.application == "need-cool" ) {
				$("#ff-min-temp-value-container").show();
			}
		}
	} else {
		var filterSettings = {};
	}

	checkQueryString()

	if(productsArray.length == fullProductsArray.length){
		$("#best-match").attr("disabled", true);
	} else {
		$("#best-match").attr("disabled", false);
	}


	// Set the current products and total products HTML

	$("#current-products").html(productsArray.length);
	$("#total-products").html(fullProductsArray.length);

	
	// Display products viewing if the arrays don't match
	if( productsArray.length !== fullProductsArray.length ) {
		$(".products-viewing").slideDown().css("display","flex");
		filterOut(productsArray);
	}

	function filterOut(productsArray) {
		var filteredOut = fullProductsArray;
		$(productsArray).each(function( index, item ){
			var id = item.id;
			filteredOut = $.grep(filteredOut, function(e){ 
			   return e.id != id;
			});
		})

		var html = "";
		for( var i=0; i<filteredOut.length; i++ ) {
			html += "<li><a href='' data-product='" + filteredOut[i].id +"'>" + filteredOut[i].name + "&nbsp;<i class='fa fa-external-link' aria-hidden='true'></i></a></li>";
		}
		$(".filtered-out").html(html);
	}

	// Handle the close product button
	$(document).on("click", ".close-product", function() {
    $(this).closest(".card-container").slideUp();
		var id = $(this).data("product");
		productsArray = $.grep(productsArray, function(e){ 
		   return e.id != id;
		});
		localStorage.setItem("productsArray", JSON.stringify(productsArray));
		$("#current-products").html(productsArray.length);
		if( productsArray.length !== fullProductsArray.length ) {
			$(".products-viewing").slideDown();
		}
		filterOut(productsArray);
	})

	//reset products array
	$(".all-products").on("click", function(e) {
		e.preventDefault();
		reset_products();
		// $(".card-container").fadeOut('fast');
	});
	function reset_products(){
		$("#tempwarn").hide();
		$("#tempwarn2").hide();
		
		productsArray = fullProductsArray;
		// console.log(productsArray);
		localStorage.setItem("productsArray", JSON.stringify(fullProductsArray));
		localStorage.removeItem("filterSettings");
		$("#ff-max-temp-value-container").val("").hide();
		$("#ff-min-temp-value-container").val("").hide();
		$("#ff-max-temp-value").val("");
		$("#ff-min-temp-value").val("");
		$("#min-flashpoint").val("");
		$(".needto-checkbox").prop("checked", false);
		$("#food-grade").prop("checked", false);
		$("#best-match").attr("disabled", true);
		$("#sortby").val("maxtemp-high");
		sort("high", "maxtemp");
		$(".products-viewing").slideUp();
		// $(".card-container").fadeOut()
		if( localStorage.getItem("tempValue") ==='c' ) {
			$(".card-container.metric").slideDown()
			console.log('c')
		} else {
			$(".card-container.standard").slideDown()
			console.log('f')
		}
		filterOut(productsArray);
	}
	// Put products back
	$(document).on("click", ".filtered-out a", function(e) {
		e.preventDefault();
		$(".card-container").fadeOut('fast');
		var id = $(this).data("product");
		var thisProduct = $.grep(fullProductsArray, function(e){ 
		   return e.id == id;
		});
		productsArray.push(thisProduct[0]);
		localStorage.setItem( "productsArray", JSON.stringify(productsArray) );
		filterOut(productsArray);

		var filteredIds = [];
		for( var x=0; x<productsArray.length; x++ ) {
			filteredIds.push(productsArray[x].id);
		}

		var tempValue = localStorage.getItem('tempValue')
		if( tempValue === 'c') {
			$(".card-container.metric").each( function( index, item ){
				var $cardContainer = $(this);
				if( $.inArray(parseInt($(item).data('product')), filteredIds) > -1 ) {
					$cardContainer.slideDown();
				}
			});
		} else {
			$(".card-container.standard").each( function( index, item ){
				var $cardContainer = $(this);
				if( $.inArray(parseInt($(item).data('product')), filteredIds) > -1 ) {
					$cardContainer.slideDown();
				}
			});
		}

		if( productsArray.length == fullProductsArray.length ) {
			$(".products-viewing").slideUp();
		}
		$("#current-products").text(productsArray.length);
	})


	//Display only selected products
	displayProducts(productsArray);
	function displayProducts(productsArray) {
		var ids = [];
		$.each(productsArray, function( index, item ){
			ids.push(item.id);
		});
		$(".card-container").each(function(){
			var id = $(this).data("product");
			if( ! ($.inArray( id, ids ) > -1) ) {
				$(this).css('display', 'none');
			}
		});
	}

	//Sort based on select box
	$("#sortby").on("change", function(){
		var sorter = $(this).val().split("-");
		sort(sorter[1], sorter[0]);
	})

	// Fluid Fiinder 

	// Application

	$(".needto-checkbox").on("click", function() {
		$("#tempwarn2").show();
		$("#need-heat-cool").val("");
		$("#ff-min-temp-value-container").hide();
		$("#ff-max-temp-value-container").hide();
		$(".heat-cool").val("");
		var $currentCheck = $(this);
		var $textBox = $currentCheck.data("action");
		$(".needto-checkbox").prop("checked", false);
		$currentCheck.prop("checked", true);

		filterSettings['application'] = $(this).attr("id");

		$(".needto-text").val("");
		if($textBox == 'maxtemp') {
			$("#ff-max-temp-value-container").show();
		} else if ($textBox == 'mintemp') {
			$("#ff-min-temp-value-container").show();
		} else {
			$("#ff-min-temp-value-container").show();
			$("#ff-max-temp-value-container").show();
		}
	})



	$('#fluid-finder').validate({
	  rules: {
	    'ff_max_temp': {number: true},
	    'ff_min_temp': {number: true},
	    'min_flashpoint': {number: true},
	  },
	    errorClass: "error",
	    errorElement: "span",
	    errorPlacement: function(error, element) {
	    error.addClass('help-inline').insertAfter(element);
	  },
	    submitHandler: function(form) {
				$("#tempwarn").hide();
				$("#tempwarn2").hide();
				if( document.getElementById('need-heat').checked && $('#ff-max-temp-value').val() =='') {
					$("#tempwarn").slideDown();
					$("#tempwarn2").show();
					return
				}
				
				if( document.getElementById('need-cool').checked && $('#ff-min-temp-value').val() =='') {
					$("#tempwarn").slideDown();
					$("#tempwarn2").show();
					return
				}
				
				if( document.getElementById('need-heat-cool').checked && ($('#ff-max-temp-value').val() =='' || $('#ff-min-temp-value').val() =='')) {
					$("#tempwarn").slideDown();
					$("#tempwarn2").show();
					return
				}

	    	productsArray = fullProductsArray;
				filterSettings['max_temp'] = $('#ff-max-temp-value').val();
				filterSettings['min_temp'] = $('#ff-min-temp-value').val();
				filterSettings['flashpoint'] = $('#min-flashpoint').val();
				filterSettings['food_grade'] = $('#food-grade').is(":checked")? "yes": "no";
				localStorage.setItem("filterSettings", JSON.stringify(filterSettings));

				var filteredProducts = [];

				var __min = -99999, __max = 99999;
				var minBulkTemp = $("#ff-min-temp-value").val() ? $("#ff-min-temp-value").val() : __min;
				var maxBulkTemp = $("#ff-max-temp-value").val() ? $("#ff-max-temp-value").val() : __max;
				var minFlashPoint = $("#min-flashpoint") ? $('#min-flashpoint').val() : null;
				var foodGrade = $("#food-grade").is(":checked") ? "yes" : "no";


				for( var i = 0; i < productsArray.length; i++ ) {
					if(localStorage.getItem('tempValue') === 'c') {
						var pMaxBulkTemp 	= Number(productsArray[i].max_temp);
						var pOptimalMinTemp = Number(productsArray[i].min_temp);
						var pFlashPoint 	= Number(productsArray[i].flashpoint);
					} else {
						var pMaxBulkTemp 	= Number(productsArray[i].max_temp_standard);
						var pOptimalMinTemp = Number(productsArray[i].min_temp_standard);
						var pFlashPoint 	= Number(productsArray[i].flashpoint_standard);
					}
					var pFoodGrade = productsArray[i].food_grade;

					if (minBulkTemp!=__min && minBulkTemp > pMaxBulkTemp) {
				    //console.log(this.products[i].title, 'bulk min temp violation (1)', minBulkTemp, this.products[i].max_bulk_temp);
				    continue;
			    }
			    if (minBulkTemp!=__min && minBulkTemp < pOptimalMinTemp) {
				    //console.log(this.products[i].title, 'min pump temp violation (1)', minBulkTemp, this.products[i].min_pump_temp);
				    continue;
			    }    
					if (maxBulkTemp!=__max && maxBulkTemp > pMaxBulkTemp) {
				    //console.log(this.products[i].title, 'bulk max temp violation (2)', maxBulkTemp, this.products[i].max_bulk_temp);
				    continue;
			    }
			    if (maxBulkTemp!=__max && maxBulkTemp < pOptimalMinTemp) {
				    //console.log(this.products[i].title, 'min pump temp violation (2)', maxBulkTemp, this.products[i].min_pump_temp);
				    continue;
			    }
					if (minFlashPoint > pFlashPoint) {
					//console.log('flash point violation', minFlashPoint,this.products[i].title, pFlashPoint);
				    continue;
					} else {
						//console.log('flash point pass', minFlashPoint,this.products[i].title, pFlashPoint);
					}
					if (foodGrade == "no" || (foodGrade == pFoodGrade)) {
						//console.log('food grade violation', this.products[i].product_name);
				    	
			    } else {
			    	continue
			    }
					filteredProducts.push(productsArray[i]);
				}
				if(filteredProducts.length === 0 ) {
					alert("Your parameters returned no results, please adjust your requirements and try again.");
					return;
				}
				productsArray = filteredProducts;
				filterOut( productsArray );
				sort("low", "viscosity");
				$("#sortby").val("viscosity-low");

				var filteredIds = [];
				for( var x=0; x<filteredProducts.length; x++ ) {
					filteredIds.push(filteredProducts[x].id);
				}
				var tempValue = localStorage.getItem('tempValue')
				$(".card-container").fadeOut('fast');
				if(tempValue === 'c') {
					$(".card-container.metric").each(function(index, item) {
						var currentCard = $(this);
						if( $.inArray($(item).data("product"), filteredIds) == -1) {
							currentCard.slideUp();
						} else {
							currentCard.slideDown();
						}
					})
				} else {
					$(".card-container.standard").each(function(index, item) {
						var currentCard = $(this);
						if( $.inArray($(item).data("product"), filteredIds) == -1) {
							currentCard.slideUp();
						} else {
							currentCard.slideDown();
						}
					})
				}

				localStorage.setItem("productsArray", JSON.stringify(productsArray));
				$("#current-products").html(productsArray.length);
				if( productsArray.length !== fullProductsArray.length ) {
					$(".products-viewing").slideDown();
					$("#best-match").attr("disabled", false);
				} else {
					$(".products-viewing").slideUp();
					$("#best-match").attr("disabled", true);
				}
				return false;
	    }
	});




function sort(direction, attribute) {

	$(".product-card-container .card-container").sort(function(a, b) {
		if( direction == 'low') {
	  	return parseInt(a.dataset[attribute]) - parseInt(b.dataset[attribute]);
	  } else {
	  	return parseFloat(b.dataset[attribute]) - parseFloat(a.dataset[attribute]);
	  }
	}).each(function() {
	  var elem = $(this);
	  elem.remove();
	  $(elem).appendTo(".product-card-container");
	});
	$('#sortby').val(attribute + '-' + direction)
}

function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, '\\$&');
    var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

function checkQueryString() {
	var maxTemp = getParameterByName('maxTemp')
	var minTemp = getParameterByName('minTemp')
	var foodGrade = getParameterByName('foodGrade')
	var flashpoint = getParameterByName('flashpoint')
	var flashsort = getParameterByName('flashpointSort')
	var fullList = getParameterByName('fullList')
	var selector = getParameterByName('selector')
	var filterExists = false

	if(selector === 'f') {
		localStorage.setItem('tempValue', 'f')
		convertTemp('f')
	} else if( selector === 'c') {
		localStorage.setItem('tempValue', 'c')
		convertTemp('c')
	}

	if( maxTemp !== null && minTemp !== null ) {
		$('#need-heat-cool').prop('checked', true)
		$('#ff-max-temp-value').val(maxTemp)
		$('#ff-min-temp-value').val(minTemp)
		$("#ff-max-temp-value-container").show();
		$("#ff-min-temp-value-container").show();
		filterExists = true
	} else if( maxTemp === null && minTemp !== null ) {
		$('#need-cool').prop('checked', true)
		$('#ff-min-temp-value').val(minTemp)
		$("#ff-min-temp-value-container").show();
		filterExists = true
	} else if( maxTemp !== null && minTemp === null ) {
		$('#need-heat').prop('checked', true)
		$('#ff-max-temp-value').val(maxTemp)
		$("#ff-max-temp-value-container").show();
		filterExists = true
	}

	if(foodGrade !== null ) {
		$('#food-grade').prop('checked', true)
		filterExists = true
	}
	if(flashpoint !== null ) {
		$('#min-flashpoint').val(flashpoint)
		filterExists = true
	}


	if(filterExists) {
		filterOnLoad(foodGrade)
	} else {
		sort("high", "maxtemp");
	}
	
	if(flashsort !== null ) {
		reset_products()
		sort('high', 'flashpoint')
	}
	if(fullList !== null ) {
		reset_products()
		sort('high', 'maxtemp')
	}
}


function filterOnLoad(foodGrade) {
	productsArray = fullProductsArray;
	localStorage.setItem("productsArray", JSON.stringify(fullProductsArray));
	localStorage.removeItem("filterSettings");

	var filteredProducts = [];	
		
	productsArray = fullProductsArray;
	// console.log(productsArray);
	localStorage.setItem("productsArray", JSON.stringify(fullProductsArray));
	localStorage.removeItem("filterSettings");
	$("#ff-max-temp-value-container").val("").hide();
	$("#ff-min-temp-value-container").val("").hide();
	$("#ff-max-temp-value").val("");
	$("#ff-min-temp-value").val("");
	$("#min-flashpoint").val("");
	$(".needto-checkbox").prop("checked", false);
	$(".products-viewing").slideUp();

  var __min = -99999, __max = 99999;
  var minBulkTemp = $("#ff-min-temp-value").val() ? $("#ff-min-temp-value").val() : __min;
  var maxBulkTemp = $("#ff-max-temp-value").val() ? $("#ff-max-temp-value").val() : __max;
  var minFlashPoint = $("#min-flashpoint") ? $('#min-flashpoint').val() : null;
  var foodGrade = $("#food-grade").is(":checked") ? "yes" : "no";


  for( var i = 0; i < productsArray.length; i++ ) {
    if(localStorage.getItem('tempValue') === 'c') {
      var pMaxBulkTemp 	= Number(productsArray[i].max_temp);
      var pOptimalMinTemp = Number(productsArray[i].min_temp);
      var pFlashPoint 	= Number(productsArray[i].flashpoint);
    } else {
      var pMaxBulkTemp 	= Number(productsArray[i].max_temp_standard);
      var pOptimalMinTemp = Number(productsArray[i].min_temp_standard);
      var pFlashPoint 	= Number(productsArray[i].flashpoint_standard);
    }
    var pFoodGrade = productsArray[i].food_grade;

    if (minBulkTemp!=__min && minBulkTemp > pMaxBulkTemp) {
      continue;
    }
    if (minBulkTemp!=__min && minBulkTemp < pOptimalMinTemp) {
      continue;
    }    
    if (maxBulkTemp!=__max && maxBulkTemp > pMaxBulkTemp) {
      continue;
    }
    if (maxBulkTemp!=__max && maxBulkTemp < pOptimalMinTemp) {
      continue;
    }
    if (minFlashPoint > pFlashPoint) {
      continue;
    } else {
    }
    if (foodGrade == "no" || (foodGrade == pFoodGrade)) {
        
    } else {
      continue
    }
    filteredProducts.push(productsArray[i]);
  }
  if(filteredProducts.length === 0 ) {
    alert("Your parameters returned no results, please adjust your requirements and try again.");
    return;
  }
  productsArray = filteredProducts;
	filterOut( productsArray );
	if( foodGrade === null ) {
		sort("low", "viscosity");
		$("#sortby").val("viscosity-low");
	} else {
		sort("high", "maxtemp");
		$("#sortby").val("maxtemp-high");
	}

  var filteredIds = [];
  for( var x=0; x<filteredProducts.length; x++ ) {
    filteredIds.push(filteredProducts[x].id);
  }
  var tempValue = localStorage.getItem('tempValue')
  $(".card-container").fadeOut('fast');
  if(tempValue === 'c') {
    $(".card-container.metric").each(function(index, item) {
      var currentCard = $(this);
      if( $.inArray($(item).data("product"), filteredIds) == -1) {
        currentCard.slideUp();
      } else {
        currentCard.slideDown();
      }
    })
  } else {
    $(".card-container.standard").each(function(index, item) {
      var currentCard = $(this);
      if( $.inArray($(item).data("product"), filteredIds) == -1) {
        currentCard.slideUp();
      } else {
        currentCard.slideDown();
      }
    })
  }

  $("#current-products").html(productsArray.length);
  if( productsArray.length !== fullProductsArray.length ) {
    $(".products-viewing").slideDown();
    $("#best-match").attr("disabled", false);
  } else {
    $(".products-viewing").slideUp();
    $("#best-match").attr("disabled", true);
  }
  return false;
}

})