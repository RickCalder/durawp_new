$(document).ready(function() {

	if (typeof page !== 'undefined' && page === 'calculators')  {
		$.getScript("/wp-content/themes/duratherm-theme/assets/dist/js/htcc.js", function(){
			var options = {};
			options.products = fullProductsArray;
			options.el = "htcc";
			if( localStorage.getItem('tempValue')=== 'c' || localStorage.getItem('tempValue') === null ) {
				options.metric = true;
			} else {
				options.metric = false;
			}
			console.log(options.metric)
	    
	    options.selectedProductId = -1;
			var htcc = new CoefficientCalculator(options);
			$.getScript("/wp-content/themes/duratherm-theme/assets/dist/js/encoding-indexes.js", function() {
				$.getScript("/wp-content/themes/duratherm-theme/assets/dist/js/encoding.js", function() {
					$.getScript("/wp-content/themes/duratherm-theme/assets/dist/js/fileSaver.js");
				});
			});

		  $('#frmHTCC').validate({
		    rules: {
			    'temperature': {required: true, number: true},
			    'pipe-diameter': {required: true, number: true},
			    'fluid-velocity-min': {required: true, number: true},
			    'fluid-velocity-max': {required: true, number: true, greaterThan: "#fluid-velocity-min-htcc"},
			    'fluid-velocity-inc': {required: true, number: true},
			    'fluid-viscosity': {required: true, number: true},
			    'fluid-specific-heat': {required: true, number: true},
			    'fluid-conductivity': {required: true, number: true},
			    'fluid-density': {required: true, number: true}
				},
		        errorClass: "error",
		        errorElement: "span",
		        errorPlacement: function(error, element) {
							error.addClass('help-inline').insertAfter(element);
						},
		        submitHandler: function(form) {
			        htcc.generateOutput();
				}
		  });

		});
	}

  $.validator.addMethod('greaterThan', function(value, element, param) {
	  return ( parseInt(value) > parseInt(jQuery(param).val()) );
	}, 'Must be greater than min' );
})