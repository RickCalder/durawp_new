"use strict"

if(localStorage.getItem('tempValue') === 'c' || localStorage.getItem('tempValue') === null) {
  var isMetric = true;
} else {
  var isMetric = false;
}
if(localStorage.getItem('tempValue') === null) {
  localStorage.setItem('tempValue', 'c')
}

$(document).ready(function() {
  var loc = window.location.href
   if( typeof page !== 'undefined' && page ==='compare') {
   }else{
     return
   }

  const queryIds = loadFromQueryString()

  if( localStorage.getItem("compareProducts") !== null ){
    var productsArray = JSON.parse(localStorage.getItem("compareProducts"));
  } else {
    var productsArray = {};
  }

  // Initialize Products
  var products = fullProductsArray;
  
  

  $('.f').on('click', function(e) {
    e.preventDefault()
    isMetric = false;
    init(compareIds);
  })
  $('.c').on('click', function(e) {
    e.preventDefault()
    isMetric = true;
    init(compareIds);
  })

  for( var i=0; i< products.length; i++ ) {
    if( products[i].name === "Duratherm 630" ) {
      var baseProd = products[i].id;
    }
  }
  if( loc.indexOf("compare-section") === -1 ) {
    for( var i=0; i < compProducts.length; i++ ) {
      products.push( compProducts[i]);
    }
  }

  var compareIds = [];
  for( var i =0; i< productsArray.length; i++ ) {
    compareIds.push(productsArray[i].id);
  }

  if( compareIds.length === 0 ) {
    compareIds = [];
    compareIds.push(baseProd);
  }

  $(".print-compare").on("click", function(e) {
    printTables();
  });

  $(".export-compare").on("click", function(e) {
    e.preventDefault();
    $(printTable).tableToCSV();
  });

  function findProduct(products, id) {
    for (var i = 0; i < products.length; i++) {
      if (products[i].id === id) {
        return products[i]
      }
    }
    return undefined
  }
  
  if(queryIds.length != 0) {
    compareIds = queryIds
  }

  init(compareIds);
  var printTable;

  function init(ids) {
    var productsToCompare = [];
    var emptyProduct = {};
    compareIds = ids
    var uniqueIds = [];
    $.each(ids, function(i, el){
        // if($.inArray(el, uniqueIds) === -1) uniqueIds.push(el);
        uniqueIds.push(el)
    });
    for (var i = 0; i < uniqueIds.length; i++) {
      var product = findProduct(products, uniqueIds[i])
      if (product) {
        productsToCompare.push(product)
      }
    }
    $.when( getTempRanges(productsToCompare) ).then( function(data) {
      var temperatureRange = data['reg'];
      var viscosityRange = data['vis']
      if(isMetric) {
        $.when(resolveProductPropertiesMetric(productsToCompare)).then( function(data2){
          renderTables(data2, temperatureRange, viscosityRange, compareIds);
          printTable = renderPrintTables(data2, temperatureRange, viscosityRange, compareIds);
        })

      }else {
        
      $.when(resolveProductProperties(productsToCompare)).then( function(data2){
        renderTables(data2, temperatureRange, viscosityRange, compareIds);
        printTable = renderPrintTables(data2, temperatureRange, viscosityRange, compareIds);
      })
      }
    })
  }

  function printTables() {
    var WinPrint = window.open('', '', 'left=0,top=0,width=800,height=900,toolbar=0,scrollbars=0,status=0,titlebar=0');
    WinPrint.document.write("<title>Duratherm Fluids Compare Products</title>");
    var css = '<style>';
        css += "body { font-family: sans-serif; -webkit-print-color-adjust: exact; }";
        css += '.page-header { display: none; }';
        css += 'table { width: 100%; border-spacing: 0; border-collapse: collapse; font-size: 0.8em;}';
        css += "td { padding: 10px 5px; }";
        css += "th { padding: 10px 5px; text-align: left;}";
        css += "tr { border-bottom:1px solid #ccc;}";
        css += ".grey-row { background: #f6f6f6 }";
        css += ".main-header { background-color: #bbb; color: #fff; font-weight: bold}";
        css += ".secondary-header { background-color: #ddd; font-weight: bold}";
        css += '</style>';
    WinPrint.document.write(css);
    WinPrint.document.write(printTable);
    WinPrint.document.close();
    WinPrint.focus();
    WinPrint.print();
    WinPrint.close(); 
  }
  var html = "<div class='product-list' data-product='0' data-name='Select Fluid'>Select Fluid</div>";
  var html2 = "<div class='product-list' data-product='0' data-name='Select Fluid'>Select Fluid</div>";

  for( var i = 0; i < products.length; i++ ) {
    html += "<div class='product-list' data-product='" + products[i].id + "' data-name='" + products[i].name + "'>" + products[i].name + "</div>"
  }
  for( var i = 0; i < products.length; i++ ) {
    if( products[i].duratherm_product === "yes" ) {
      html2 += "<div class='product-list' data-product='" + products[i].id + "' data-name='" + products[i].name + "'>" + products[i].name + "</div>"
    }
  }
  $(".all-fluids").html(html);
  $(".fluids1").html(html2);
  

  // Dropdown List Animations
  $(".compare-header").on("click", function(){
    var thisOne = $(this).closest('.compare-column').find('.fluids');
    var visible = $(this).closest('.compare-column').find('.fluids').css("display") == "block" ? "yes" : "no";

    $(".fluids").hide();
    if( visible == "no" ) {
      thisOne.slideDown();
      $(".compare-nav").css("z-index", "6001");
    }

    checkForFluids();
  });

  $(document).on("click", ".product-list", function(){
    $(".fluids").hide();
    checkForFluids();

    var header = $(this).closest(".fluids").data("header");
    $("#" + header).data("product", $(this).data("product"));
    $(this).closest(".prod-header").data("product", $(this).data("product"));
    compareIds.push( $(this).data('product'))
    var newProducts = [];
    $(".prod-header").each(function(index, elem) {
      newProducts.push($(elem).data("product"));
    })
    parameterizeArray(newProducts)
    init(newProducts)
  })
});

function parameterizeArray(arr ) {
  var queryString = '?'
  var x = 1
  for( var x=0; x <= arr.length -1; x++ ) {
    if( arr[x] == 0) continue
    queryString += 'prod' + (parseInt(x) +1) + '=' +arr[x] + '&'
  }
  history.pushState({}, "", queryString);
}

function loadFromQueryString(){
  queryIds = []
  for( var x = 1; x <= 4; x++ ) {
    if(getQueryString('prod' + x) != null){
    queryIds.push(parseInt(getQueryString('prod' + x)))
    }
  }
  return queryIds 
  
}

function getQueryString(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, '\\$&');
  var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, ' '));
}

// Start functions


function checkForFluids() {
  var fluidsVisible = 0;   

  $(".fluids").each(function(){
    if($(this).css("display") == "block") {
      fluidsVisible++;
    }
  })
  if( fluidsVisible > 0 ) 
    $(".compare-nav").css("height", "600px");        
   else {
    $(".compare-nav").css("height", "60px");
  }   
}


function renderPrintTables(products, temperatureRange, viscosityRange, compareIds) {
  var html = "<table class='print-compare-table'><tr><th>Products</th>";

  for( var i=0; i<products.length; i++) {
    html += "<th>" + products[i].name + "</th>";
  }
  html += "<tr class='main-header'><td colspan='" + (products.length + 1) + "'><strong>Temperature Ratings</strong></td>";
  html += "</tr><tr><td>Max Bulk Temp</td>";

  for( var i=0; i<products.length; i++) {
    if(isMetric) {
      var max_temp = passTemp(products[i].max_temp);
    } else {
      var max_temp = passTemp(products[i].max_temp_standard);
    }
    html += "<td>" + max_temp + "</td>";
  }
  html += "</tr><tr class='grey-row'><td>Max Film Temp</td>";

  for( var i=0; i<products.length; i++) {
    if(isMetric) {
      var film_temp = passTemp(products[i].film_temp);
    } else {
      var film_temp = passTemp(products[i].film_temp_standard);
    }
    html += "<td>" + film_temp + "</td>";
  }
  html += "</tr><tr><td>Pour Point</td>";

  for( var i=0; i<products.length; i++) {
    if(isMetric) {
      var pour_point = passTemp(products[i].pour_point);
    } else {
      var pour_point = passTemp(products[i].pour_point_standard);
    }
    html += "<td>" + pour_point + "</td>";
  }
  html += "<tr class='main-header'><td colspan='" + (products.length + 1) + "'><strong>Safety Data</strong></td>";
  html += "</tr><tr><td>Flash Point</td>";

  for( var i=0; i<products.length; i++) {
    if(isMetric) {
      var flashpoint = passTemp(products[i].flashpoint);
    } else {
      var flashpoint = passTemp(products[i].flashpoint_standard);
    }
    html += "<td>" + flashpoint + "</td>";
  }
  html += "</tr><tr class='grey-row'><td>Fire Point</td>";

  for( var i=0; i<products.length; i++) {
    if(isMetric) {
      var fire_point = passTemp(products[i].fire_point);
    } else {
      var fire_point = passTemp(products[i].fire_point_standard);
    }
    html += "<td>" + fire_point + "</td>";
  }
  html += "</tr><tr><td>Autoignition Temp</td>";

  for( var i=0; i<products.length; i++) {
    if(isMetric) {
      var auto_ignition_temp = passTemp(products[i].auto_ignition_temp);
    } else {
      var auto_ignition_temp = passTemp(products[i].auto_ignition_temp_standard);
    }
    html += "<td>" + auto_ignition_temp + "</td>";
  }
  html += "<tr class='main-header'><td colspan='" + (products.length + 1) + "'><strong>Thermal Properties</strong></td>";
  html += "</tr><tr><td><strong>Thermmal Expansion Coefficient</strong></td>";

  for( var i=0; i<products.length; i++) {
    if(typeof products[i].thermal_expansion_coefficient_standard !== 'undefined') {
      if(isMetric) {
        var thermal_expansion_coefficient = passTemp(products[i].thermal_expansion_coefficient_standard);
      } else {
        var thermal_expansion_coefficient = passTemp(products[i].thermal_expansion_coefficient);
      }
    } else {
      var thermal_expansion_coefficient = 'N/A'
    }
    html += "<td>" + thermal_expansion_coefficient + "</td>";
  }
  if(isMetric) {
    html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Thermal Conductivity (W/m·k)</strong></td></tr>";
  } else {
    html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Thermal Conductivity (BTU/hr·ft·F°)</strong></td></tr>";
  }
  var tc = generatePrintTables( "thermal_conductivity", products, temperatureRange );
  html += tc;

  if(isMetric) {
    html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Heat Capacity (kJ/kg·K)</strong></td></tr>";
  } else {
    html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Heat Capacity (BTU/lb·F°)</strong></td></tr>";
  }
  var hc = generatePrintTables( "heat_capacity", products, temperatureRange );
  html += hc;

  html += "<tr class='main-header'><td colspan='" + (products.length + 1) + "'><strong>Physical Properties</strong></td>";
  html += "</tr><tr><td><strong>Chemical Composition</strong></td>";

  for( var i=0; i<products.length; i++) {
    var chemical_composition = products[i].chemical_composition;
    html += "<td>" + chemical_composition + "</td>";
  }

  html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Kinematic Viscosity (cSt)</strong></td></tr>";
  var kv = generatePrintTables( "kinematic_viscosity", products, viscosityRange );
  html += kv;

  if(isMetric) {
    html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Density (kg/L)</strong></td></tr>";
  } else {
    html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Density (lb/ft³)</strong></td></tr>";
  }
  var den = generatePrintTables( "density", products, temperatureRange );
  html += den;

  if(isMetric) {
    html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Vapor Pressure (kPa)</strong></td></tr>";
  } else {
    html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Vapor Pressure (psia)</strong></td></tr>";
  }
  var vp = generatePrintTables( "vapour_pressure", products, temperatureRange );
  html += vp;
  html += "<tr class='secondary-header'><td colspan='" + (products.length + 1) + "'><strong>Distillation Range</strong></td>";
  html += "</tr><tr><td>10%</td>";

  for( var i=0; i<products.length; i++) {
    if(isMetric) {
      var distillation_range_10 = passTemp(products[i].distillation_range_10);
    } else {
      var distillation_range_10 = passTemp(products[i].distillation_range_10_standard);
    }
    html += "<td>" + distillation_range_10 + "</td>";
  }
  html += "</tr><tr><td>90%</td>";

  for( var i=0; i<products.length; i++) {
    if(isMetric) {
      var distillation_range_90 = passTemp(products[i].distillation_range_90);
    } else {
      var distillation_range_90 = passTemp(products[i].distillation_range_90_standard);
    }
    html += "<td>" + distillation_range_90 + "</td>";
  }
  html += "</tr><tr><td><strong>Average Molecular Weight</strong></td>";

  for( var i=0; i<products.length; i++) {
    var average_molecular_weight = products[i].average_molecular_weight;
    html += "<td>" + average_molecular_weight + "</td>";
  }
  if(isMetric) {
    html += "</tr><tr><td><strong>Specific Gravity 16 <span class='metric'>&deg;C</span></strong></td>";
  } else {
    html += "</tr><tr><td><strong>Specific Gravity 60 <span class='standard'>&deg;F</<span></strong></td>";
  }

  for( var i=0; i<products.length; i++) {
    var specific_gravity = products[i].specific_gravity != "N/A" ? roundNumber(products[i].specific_gravity, 3) : "N/A";
    html += "<td>" + specific_gravity + "</td>";
  }

  html +="</table>";
  return html;
}

function generatePrintTables( param, products, temperatureRange ) {
  var html = "";
  for( var i=0; i<temperatureRange.length; i++ ) {
    var rowClass = i % 2 ? "grey-row": "";
    html += "<tr class='" + rowClass + "'>";
    html += "<td>" + passTemp(temperatureRange[i]) + "</td>";
    for( var j=0; j<=3; j++ ) {
      if( ( _.isEmpty(products[j]))  ) {
      } else if(isMetric === false) {
        var currentTemp = parseInt(temperatureRange[i]);
        for( var x=0; x< products[j].fluid_data_standard.length; x++ ){
          var productTemp = parseInt(products[j].fluid_data_standard[x].temperature);
          if( currentTemp == productTemp ){
            var temp = "<td>" + roundNumber(products[j].fluid_data_standard[x][param], 3) +"</td>";
            break;
          } else if( products[j].duratherm_product !== "yes") {
            var temp = "<td>N/A</td>";
          } else {
            var temp = "<td>Out of Range</td>";
          }
        }
        html += temp;
      } else {
        var currentTemp = parseInt(temperatureRange[i]);
        for( var x=0; x< products[j].fluid_data.length; x++ ){
          if(typeof products[j].fluid_data[x] == 'undefined') {
            continue;
          }
          var productTemp = parseInt(products[j].fluid_data[x].temperature);
          if( currentTemp == productTemp ){
            var temp = "<td>" + roundNumber(products[j].fluid_data[x][param], 3) +"</td>";
            break;
          } else if( products[j].duratherm_product !== "yes") {
            var temp = "<td>N/A</td>";
          } else {
            var temp = "<td>Out of Range</td>";
          }
        }
        html += temp;

      }
    }
    html += "</tr>";
  }
    return html;
}

function renderTables(products, temperatureRange, viscosityRange, compareIds) {
  
  $("#tc").html("");
  $("#hc").html("");
  $("#kv").html("");
  $("#den").html("");
  $("#vp").html("");

  for( var x=1; x<=4; x++ ) {
    // Product Dropdown
    $("#name-" + x).text('Select Fluid');
    $("#name-" + x).data("product", "0");
    $("#name-floater-" + x).text("");
    //temps
    $("#maxbulk-" + x).html("");
    $("#maxfilm-" + x).html("");
    $("#pourpoint-" + x).html("");
    // safety data
    $("#flashpoint-" + x).html("");
    $("#firepoint-" + x).html("");
    $("#autoignition-" + x).html("");
    // Thermal Properties
    $("#thermalexpansion-" + x).html("");
    // Physical properties
    $("#chemical-composition-" + x).html("");
    $("#appearance-" + x).html("");
    $("#dist10-" + x).html("");
    $("#dist90-" + x).html("");
    $("#amw-" + x).html("");
    $("#sg-" + x).html("");
  }
  // Thermal Conductivity
  generateTables( "tc", "thermal_conductivity", products, temperatureRange );

  //Heat Capacity
  generateTables( "hc", "heat_capacity", products, temperatureRange );

  //Kinematic Viscosity
  generateTables( "kv", "kinematic_viscosity", products, viscosityRange );

  //Density
  generateTables( "den", "density", products, temperatureRange );

  //Density
  generateTables( "vp", "vapour_pressure", products, temperatureRange );


  for( var i=0; i<products.length; i++) {
    var x = i + 1;
    //reset values

    if( products[i].duratherm_product === 'yes' ) {
      var max_temp = cnvTemp(products[i].max_temp_standard);
      var film_temp = cnvTemp(products[i].film_temp_standard);
      var pour_point = cnvTemp(products[i].pour_point_standard)
      var flashpoint = cnvTemp(products[i].flashpoint_standard)
      var fire_point = cnvTemp(products[i].fire_point_standard)
      var auto_ignition_temp = cnvTemp(products[i].auto_ignition_temp_standard)
    } else {    var max_temp = cnvTempReverse(products[i].max_temp);
      var film_temp = cnvTempReverse(products[i].film_temp);
      var pour_point = cnvTempReverse(products[i].pour_point)
      var flashpoint = cnvTempReverse(products[i].flashpoint)
      var fire_point = cnvTempReverse(products[i].fire_point)
      var auto_ignition_temp = cnvTempReverse(products[i].auto_ignition_temp)

    }
    if(isMetric) {
      var thermal_expansion_coefficient = products[i].thermal_expansion_coefficient_standard + ' %/&deg;C';
    } else {
      var thermal_expansion_coefficient = products[i].thermal_expansion_coefficient + ' %/&deg;F';
    }
    var chemical_composition = products[i].chemical_composition;
    var distillation_range_10 = cnvTempReverse(products[i].distillation_range_10);
    var distillation_range_90 = cnvTempReverse(products[i].distillation_range_90);
    var average_molecular_weight = products[i].average_molecular_weight;
    var specific_gravity = products[i].specific_gravity != "N/A" ? roundNumber(products[i].specific_gravity, 3) : "N/A";
    if( products[i].duratherm_product == "yes" ) {
      var appearance = '';
    } else {
      var appearance ="";
    }

    // Product Dropdown
    $("#name-" + x).text(products[i].name);
    $("#name-" + x).data("product", products[i].id);
    $("#name-floater-" + x).text(products[i].name);
    //temps
    $("#maxbulk-" + x).html(max_temp);
    $("#maxfilm-" + x).html(film_temp);
    $("#pourpoint-" + x).html(pour_point);
    // safety data
    $("#flashpoint-" + x).html(flashpoint);
    $("#firepoint-" + x).html(fire_point);
    $("#autoignition-" + x).html(auto_ignition_temp);
    // Thermal Properties
    $("#thermalexpansion-" + x).html(thermal_expansion_coefficient);
    // Physical properties
    $("#chemical-composition-" + x).html(chemical_composition);
    $("#appearance-" + x).html(appearance);
    $("#dist10-" + x).html(distillation_range_10);
    $("#dist90-" + x).html(distillation_range_90);
    $("#amw-" + x).html(average_molecular_weight);
    $("#sg-" + x).html(specific_gravity);
  }

}

function cnvTemp( temp ) {
  if( temp == "N/A" ) return temp;
  if( isMetric ) {
    var result = (temp-32) * 5/9;
    return parseInt(result + (result < 0 ? -.5 : .5)) + "&deg;C";
  }
  return temp + "&deg;F";
}
function cnvTempReverse( temp ) {
  if( temp == "N/A" || temp == null) return "N/A";
  if( isMetric ) {
    return temp + '&deg;C';
  }
  var result = (temp*1.8) + 32;
  return parseInt(result + (result < 0 ? -.5 : .5)) + '&deg;F';
}
function cnvTempRaw( temp ) {
  if( temp == "N/A" ) return temp;
  if( isMetric ) {
    return temp
  }
  var result = (temp*1.8) + 32;
  return parseInt(result + (result < 0 ? -.5 : .5))
}

function cnvViscosity( visc ) {

}

function passTemp( temp ){
  if( temp == "N/A" ) return temp;
  if(isMetric) {
    return temp  + "<span class='metric'>&deg;C</span>";
  }
  return temp  + "<span class='standard'>&deg;F</span>";
}

function cnvTec ( tec ) {
  if( tec == "N/A" ) return tec;
  if( isMetric ) {
    return roundNumber( tec * 1.8, 4 ) + "&#37;/&deg;C";
  }
  return roundNumber( tec, 4 ) + "&#37;/&deg;F";
}

function passTec ( tec ) {
  if( tec == "N/A" ) return tec;
  if(isMetric) {
    return roundNumber( tec * 1.8, 4 ) + "&#37;/<span class='metric'>&deg;C</span>";
  }
  return roundNumber( tec * 1.8, 4 ) + "&#37;/<span class='standard'>&deg;F</span>";
}

function getTempRanges(products) {  
  var tempRange = []; 
  var visRange = [];
  for( var i=0; i< products.length; i++ ) {
    if( ! _.isEmpty(products[i]) ) {
      var prodRange = products[i].temperature_range.split(",");
      for( var x=0; x< prodRange.length; x++ ) {
        tempRange.push( parseInt(prodRange[x]) );
      }
    }
  }

  for( var i=0; i< products.length; i++ ) {
    if( ! _.isEmpty(products[i]) ) {
      var prodRange = products[i].viscosity_range.split(",");
      for( var x=0; x< prodRange.length; x++ ) {
        visRange.push( parseInt(prodRange[x]) );
      }
    }
  }
  var temperatureRange = [];
  temperatureRange['reg'] = sort_unique(tempRange);
  temperatureRange['vis'] = sort_unique(visRange);
  return temperatureRange;
}

function sort_unique(arr) {
    if (arr.length === 0) return arr;
    arr = arr.sort(function (a, b) { return a*1 - b*1; });
    var ret = [cnvTempRaw(arr[0])];
    for (var i = 1; i < arr.length; i++) { // start loop at 1 as element 0 can never be a duplicate
      if (arr[i-1] !== arr[i]) {
        ret.push( cnvTempRaw(arr[i]) )
      }
    }
    return ret;
}

function roundNumber(num, dec) {
  if( num === "N/A" ) return num;
  return Math.round(num * Math.pow(10, dec)) / Math.pow(10, dec);
}

function getProductsWithRange( products1, temperatureRange ) {  return products1.map(function( product ) {
    product.fluid_data_standard = _.filter(product.fluid_data_standard, function(fluid_data_standard_item) {
       return _.includes(temperatureRange, fluid_data_standard_item.temperature);
    });
    return product;
  });
}

function resolveProductProperties(products) {
  // loop through the products and create a temperature value for each degree
    for(var i=0; i<products.length; i++) {
    if( _.isEmpty(products[i]) ) {
      continue;
    }
    if( products[i].duratherm_product !== "yes") {
      continue;
    }

      var temperatureValues = [];      
        for(var j=0; j<products[i].fluid_data_standard.length-1; j++) {
          var current = products[i].fluid_data_standard[j];
          var next = products[i].fluid_data_standard[j+1];
          for(var k=parseInt(current.temperature); k<parseInt(next.temperature); k++) {
  
            var pct = (k-parseInt(current.temperature))/(parseInt(next.temperature) - parseInt(current.temperature));
            temperatureValues.push({
              temperature:k,
              density:                parseFloat(current.density)+((parseFloat(next.density)-parseFloat(current.density))*pct),
              kinematic_viscosity:    parseFloat(current.kinematic_viscosity)+((parseFloat(next.kinematic_viscosity)-parseFloat(current.kinematic_viscosity))*pct),
              dynamic_viscosity:      parseFloat(current.dynamic_viscosity)+((parseFloat(next.dynamic_viscosity)-parseFloat(current.dynamic_viscosity))*pct),
              thermal_conductivity:   parseFloat(current.thermal_conductivity)+((parseFloat(next.thermal_conductivity)-parseFloat(current.thermal_conductivity))*pct),
              heat_capacity:          parseFloat(current.heat_capacity)+((parseFloat(next.heat_capacity)-parseFloat(current.heat_capacity))*pct),
              vapour_pressure:        parseFloat(current.vapour_pressure)+((parseFloat(next.vapour_pressure)-parseFloat(current.vapour_pressure))*pct)
            });
          }
        }
          temperatureValues.push(products[i].fluid_data_standard[j]);
          products[i].fluid_data_standard = temperatureValues;

    }
    return products;

}
function resolveProductPropertiesMetric(products) {
  // loop through the products and create a temperature value for each degree
    for(var i=0; i<products.length; i++) {
    if( _.isEmpty(products[i]) ) {
      continue;
    }
    if( products[i].duratherm_product !== "yes") {
      continue;
    }

      var temperatureValues = [];
          for(var j=0; j<products[i].fluid_data.length-1; j++) {
            var current = products[i].fluid_data[j];
            var next = products[i].fluid_data[j+1];
            if(typeof next == 'undefined') {
              continue
            }
            for(var k=parseInt(current.temperature); k<parseInt(next.temperature); k++) {
              var pct = (k-parseInt(current.temperature))/(parseInt(next.temperature) - parseInt(current.temperature));
              temperatureValues.push({
                temperature:k,
                density:                parseFloat(current.density)+((parseFloat(next.density)-parseFloat(current.density))*pct),
                kinematic_viscosity:    parseFloat(current.kinematic_viscosity)+((parseFloat(next.kinematic_viscosity)-parseFloat(current.kinematic_viscosity))*pct),
                dynamic_viscosity:      parseFloat(current.dynamic_viscosity)+((parseFloat(next.dynamic_viscosity)-parseFloat(current.dynamic_viscosity))*pct),
                thermal_conductivity:   parseFloat(current.thermal_conductivity)+((parseFloat(next.thermal_conductivity)-parseFloat(current.thermal_conductivity))*pct),
                heat_capacity:          parseFloat(current.heat_capacity)+((parseFloat(next.heat_capacity)-parseFloat(current.heat_capacity))*pct),
                vapour_pressure:        parseFloat(current.vapour_pressure)+((parseFloat(next.vapour_pressure)-parseFloat(current.vapour_pressure))*pct)
              });
            }
          }
            temperatureValues.push(products[i].fluid_data_standard[j]);
            products[i].fluid_data = temperatureValues;
          }
      
    return products;

}

// Fix column labels

var $parent = $(".compare-wrapper");
var $el = $(".compare-floater");
$parent.scroll(function() {
  if( $parent.scrollTop() > 50 )  {
    // $(".fluids").slideUp();
    $el.fadeIn().css('display', 'flex');
  } else {
    $el.fadeOut();
  }
  $el.css('top', $parent.scrollTop());
});

function generateTables( el, param, products, temperatureRange ) {
  var html = "";
  var el = $("#" + el);
  if( !isMetric ) {

    for( var i=0; i<temperatureRange.length; i++ ) {
      var rowClass = i % 2 ? " grey-row": "";
      html += "<div class='data-row" + rowClass + "'>";
      html += "<div class='table-label'>" + temperatureRange[i] + "&deg;F</div>";
      for( var j=0; j<=3; j++ ) {
        if( ( _.isEmpty(products[j]))  ) {
          html += "<div class='table-data'><span> </span></div>";
        } else {
          var currentTemp = parseInt(temperatureRange[i]);
          for( var x=0; x< products[j].fluid_data_standard.length; x++ ){
            var productTemp = parseInt(products[j].fluid_data_standard[x].temperature);
            if( currentTemp == productTemp ){
              var temp = "<div class='table-data'><span>" + roundNumber(products[j].fluid_data_standard[x][param], 3) +"</span></div>";
              break;
            } else if( products[j].duratherm_product !== "yes") {
              var temp = "<div class='table-data'><span>N/A</span></div>";
            } else {
              var temp = "<div class='table-data'><span>Out of Range</span></div>";
            }
          }
          html += temp;
        }
      }
      html += "</div>";
    }
  } else {
    for( var i=0; i<temperatureRange.length; i++ ) {
      var rowClass = i % 2 ? " grey-row": "";
      html += "<div class='data-row" + rowClass + "'>";
      html += "<div class='table-label'>" + temperatureRange[i] + "&deg;C</div>";
      for( var j=0; j<=3; j++ ) {
        if( ( _.isEmpty(products[j])) || typeof products[j] === 'undefined'  ) {
          html += "<div class='table-data'><span> </span></div>";
        } else {
          var currentTemp = parseInt(temperatureRange[i]);
          for( var x=0; x< products[j].fluid_data.length; x++ ){
            if(typeof products[j].fluid_data[x] == 'undefined') {
              continue;
            }
            var productTemp = parseInt(products[j].fluid_data[x].temperature);
            if( currentTemp == productTemp ){
              var temp = "<div class='table-data'><span>" + roundNumber(products[j].fluid_data[x][param], 3) +"</span></div>";
              break;
            } else if( products[j].duratherm_product !== "yes") {
              var temp = "<div class='table-data'><span>N/A</span></div>";
            } else {
              var temp = "<div class='table-data'><span>Out of Range</span></div>";
            }
          }
          html += temp;
        }
      }
      html += "</div>";
    }
  }
  el.html(html);
}

jQuery.fn.tableToCSV = function() {
    
    var clean_text = function(text){
        text = text.replace(/"/g, '""');
        text = text.replace("Â","");
        return '"'+text+'"';
    };
    
  $(this).each(function(){
      var table = $(this);
      var caption = "FluidComparisonTable";
      var title = [];
      var rows = [];

      $(this).find('tr').each(function(){
        var data = [];
        $(this).find('th').each(function(){
          var text = clean_text($(this).text());
          title.push(text);
          });
        $(this).find('td').each(function(){
          var text = clean_text($(this).text());
          data.push(text);
          });
        data = data.join(",");
        rows.push(data);
        });
      title = title.join(",");
      rows = rows.join("\n");

      var csv = title + rows;
      var textEncoder = new CustomTextEncoder('windows-1252', {NONSTANDARD_allowLegacyEncoding: true});
      var csvContentEncoded = textEncoder.encode([csv]);
      var blob = new Blob([csvContentEncoded], {type: 'text/csv;charset=windows-1252;'});
      saveAs(blob, 'Fluid_Comparison_Table.csv');
  });
    
};