

(function($) {
  var logoscroll
  $('[data-toggle="popover"]').popover({
    container: 'body'
  }); 
  if( window.location.pathname === '/' ) {
    $('.dura-diff-container > div:gt(0)').hide()
    setInterval(function() {
      $('.dura-diff-container > div:first')
        .hide()
        .next()
        .slideDown(500)
        .end()
        .remove()
        .appendTo('.dura-diff-container');
    }, 2000);

    $('.testimonial-container > div:gt(0)').hide()
    setInterval(function() {
      $('.testimonial-container > div:first')
        .fadeOut(500)
        .next()
        .fadeIn(500)
        .end()
        .remove()
        .appendTo('.testimonial-container');
    }, 3000);

  }
  //Q&A
  if( window.location.pathname == '/resources/faq/' || window.location.pathname == '/open-bath/') {
    $(".question").on("click", function() {
      if(! $(this).hasClass('active') ) {
        $('.question').removeClass('active')
        $(this).addClass('active')
        $(".question").find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
        $(this).find('i').removeClass('fa-chevron-down').addClass('fa-chevron-up')
        $(".answer").slideUp().removeClass('active')
        $(this).next().slideDown()
      } else {
        $(this).removeClass('active')
        $(this).find('i').removeClass('fa-chevron-up').addClass('fa-chevron-down')
        $(this).next().slideUp()
      }
  
    })
    
   
  }
   
  
  var equalheight = function(container){
  var currentTallest = 0,
       currentRowStart = 0,
       rowDivs = new Array(),
       $el,
       topPosition = 0;
   $(container).each(function() {
     $el = $(this);
     $($el).height('auto')
     topPostion = $el.position().top;
  
     if (currentRowStart != topPostion) {
       for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
         rowDivs[currentDiv].height(currentTallest);
       }
       rowDivs.length = 0; // empty the array
       currentRowStart = topPostion;
       currentTallest = $el.height();
       rowDivs.push($el);
     } else {
       rowDivs.push($el);
       currentTallest = (currentTallest < $el.height()) ? ($el.height()) : (currentTallest);
    }
     for (currentDiv = 0 ; currentDiv < rowDivs.length ; currentDiv++) {
       rowDivs[currentDiv].height(currentTallest);
     }
   }); 
  }
  
  var debounce = function(func, wait, immediate) {
    var timeout;
    return function() {
      var context = this, args = arguments;
      var later = function() {
        timeout = null;
        if (!immediate) func.apply(context, args);
      };
      var callNow = immediate && !timeout;
      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
      if (callNow) func.apply(context, args);
    };
  };
  
  
  
  $(window).on('load',function() {
    equalheight('.fluid-box .inner')
    equalheight('.tech-box .inner')
    equalheight('.calc-box .inner')
    menuScroll()
  });
  
  // $(window).scroll(function() {
  // });
  
  $(window).on('resize', function() {
    equalheight('.fluid-box .inner')
    equalheight('.tech-box .inner')
  });

  
   if( $(".client-logos").length !==0 ) {
    logoscroll = setTimeout(function(){
      var scroller = $('#scroller div.innerScrollArea');
      var scrollerContent = scroller.children('ul');
      scrollerContent.children().clone().appendTo(scrollerContent);
      var curX = 0;
      scrollerContent.children().each(function(){
          var $this = $(this);
          $this.css('left', curX);
          curX += $this.outerWidth(true);
      });
      var fullW = curX / 2;
      var viewportW = scroller.width();
  
      // Scrolling speed management
      var controller = {curSpeed:0, fullSpeed:1};
      var $controller = $(controller);
      var tweenToNewSpeed = function(newSpeed, duration) {
        $controller.stop(true).animate({curSpeed:newSpeed}, duration);
      };
  
  
      // Scrolling management; start the automatical scrolling
      var doScroll = function() {
          var curX = scroller.scrollLeft();
          var newX = curX + controller.curSpeed;
          if (newX > fullW*2 - viewportW) {
            newX -= fullW;
          }
          scroller.scrollLeft(newX);
      };
      var speed = 10
      setInterval(doScroll, speed);
      tweenToNewSpeed(controller.fullSpeed);
    },300)
  
    }
  var newY = $(document).scrollTop()
  
  var menuScroll = function() {
    var top = $(document).scrollTop()
  
    if( newY === top ) {
      newY = top
    } else if( top >= newY ) {
      menuDown()
      newY = top
    } else {
      menuUp(top)
      newY = top
    }
  
  }
  $(window).on('load', function(){
    var top = $(document).scrollTop()
    if( top >= 100 ) {
      menuDown()
    }
  })
  
  var menuDown = function() {
    var topPlus = $('.admin-bar').length ? 32 : 0
    // $('.compare-nav').css({'top': 79 + topPlus, 'position': 'fixed'} )
    // $('.compare-back').css({'top': 79 + topPlus, 'position': 'fixed'} )
  
    $('.header-container').css('background-color','rgba(255,255,255,1)')
  
  }
  
  var menuUp = function(top) {
    var topPlus = $('.admin-bar').length ? 32 : 0
    // $('.compare-nav').css({'top': 128 + topPlus, 'position': 'fixed'} )
    // $('.compare-back').css({'top': 128 + topPlus, 'position': 'fixed'} )
  
    if( top <= 100) {
      $('.header-container').css('background-color','rgba(255,255,255,0.9)')
      // $('.compare-nav').css({'top': 0, 'position': 'absolute'} )
      // $('.compare-back').css({'top': 0, 'position': 'absolute'} )
    }
  }
  //testimonials rotator
  if(typeof testimonials != 'undefined') {
    var interval = testimonials[0].post_content.length * 40;
    $("#testimonial").html(testimonials[0].post_content);
    $("#testimonial-author").html(testimonials[0].testimonial_author);
    var tCounter = 1;
  
    function callback() {
      interval = (testimonials[tCounter].post_content.length * 40)
      $("#testimonial").html(testimonials[tCounter].post_content);
      $("#testimonial-author").html(testimonials[tCounter].testimonial_author);
      if(tCounter < testimonials.length - 1) {
        tCounter++;
      } else {
        tCounter = 0;
      }
      setTimeout( callback, interval );
    }
    setTimeout(callback, interval);
  }
  // $(document).on('scroll', debounce(menuScroll, 50))
  // $(document).on('scroll', debounce(compareScroll, 50))

  $(document).on('scroll', function(){
    // console.log(el.outerHeight())
    menuScroll()
    compareScroll()
    // if($(window).scrollTop() === 0 ) {
    //   $('.header-container').css('background-color: rgba(255,255,255,0.9)')
    // }
  })
  function compareScroll() {
    if( typeof page !== 'undefined' && page ==='compare') {
    }else{
      return
    }
    if( $(window).scrollTop() > 450 ) {
      $('#compare-button-container').slideDown()
    } else {
      $('#compare-button-container').slideUp()
    }
    var el = $('.inner-header');
    var compareNav = $('.compare-nav').offset().top - $(window).scrollTop()
    if( compareNav <= el.outerHeight() + 20 && $(window).scrollTop() > 200) {
      $('.compare-back').css({'position': 'fixed', 'top': el.outerHeight() + 20})
      $('.compare-nav').css({'position': 'fixed', 'top': el.outerHeight() + 20})
    } else {
      $('.compare-back').css({'position': 'absolute', 'top': 0}) 
      $('.compare-nav').css({'position': 'absolute', 'top': 0})    
    }
  }
  /**
   * Product page select
  */
  
  if( typeof productPage != 'undefined') {
    $('#product-changer').on('change', function(e) {
      var newPage = $(this).find(':selected').val()
      if( newPage === '' ) {
        return false
      } else if(newPage === 'all-products') {
        window.location.href = window.location.protocol + '/' +window.location.hostname + '/heat-transfer-fluid' 
        return
      } else if(newPage === 'open-bath') {
        window.location.href = window.location.protocol + '/' +window.location.hostname + '/open-bath' 
      } else if(newPage === 'system-cleaners') {
        window.location.href = window.location.protocol + '/' +window.location.hostname + '/system-cleaners' 
      } else if(newPage === 'glycol-coolants') {
        window.location.href = window.location.protocol + '/' +window.location.hostname + '/glycol-coolants' 
      }
      window.location.href = window.location.protocol + '/' +window.location.hostname + '/' + newPage
      return
    })
  }
 

  $('.logo-trigger').on('click', function(e) {
    e.preventDefault()
    var fullHeight =$('#client-logo').height() + 70
    var closedHeight
    if( $(window).width() > 900 ) {
      closedHeight = 300
    } else {
      closedHeight = 200
    }
    if( $('.logo-container').hasClass('logo-closed') ) {
      $('.logo-container').animate({
        height: fullHeight
      })
      $('.logo-container').removeClass('logo-closed').addClass('logo-open')
      $('.trigger-text').html('Less <i class="fa fa-chevron-up"></i>')
    } else {
      $('.logo-container').animate({
        height: closedHeight
      })
      $('.logo-container').addClass('logo-closed').removeClass('logo-open')
      $('.trigger-text').html('More <i class="fa fa-chevron-down"></i>')
    }
  })

  //menu active state

  var productPages = [
    'products',
    'heat-transfer-fluid',
    'open-bath',
    'system-cleaners',
    'glycol-coolants',
    'industry',
    'list'
  ]
  
  var whyPages = [
    'why'
  ]
  var resourcePages = [
    'resources',
    'news',
    'duratherm-keeps-atwood-cooking',
    'fluid-system-clean-puts-used-equipment-back-online',
    'duratherm-keeps-candle-burning-small-business',
    'duratherms-performance-offers-perfect-solution',
    'basic-considerations-fluid-selection',
    'dealing-water-heat-transfer-system',
    'understanding-flash-fire-autoignition-points',
    'fluid-selection-systems-250-gallons',
    'guide-fluid-selection-systems-250-gallons-electrically-heated',
    'lowdown-open-baths-heat-transfer-fluids',
    'avoiding-oxidation-thermal-degradation',
    'start-shut-procedures',
    'understanding-fluid-degradation-2',
    'understanding-heat-transfer-fluids-extruder-2',
    'looking-trouble-just-add-water',
    'need-clean-system',
    'custom-fluids-off-shelf-wont',
    'choosing-proper-pump-heat-transfer-system',
    'filter-strainer-selection-basic-overview',
    'fluid-phraseology',
    'watts-robbing-service-life',
    'expansion-tanks-big',
    'insulation-heat-transfer-systems',
    'thermal-fluids-material-compatibility',
    'things-consider-working-open-baths',
    'sludge-series-part-iii-clean-sludge-system',
    'sludge-series-part-ii-avoid-sludge-system',
    'sludge-series-part-causes-sludge-system',
    'duratherm-fluid-analysis-best-just-got-better',
    'leaky-system-simple-steps-help-seal-deal',
    'fluid-maintenance-small-systems',
    'fluid-maintenance-large-systems',
    'whats-difference-flush-system-cleaner',
    'used-fluid-drum-disposal',
    'system-cleaners-one-right',
    'proper-fluid-maintenance-change-procedures',
    'understanding-fluid-degradation',
    'understanding-heat-transfer-fluids-extruder',
    'preparation-startup-new-systems',
    'duratherm-recommended-sampling-procedures-intervals'
  ]

  var currentPage = window.location.pathname 
  var currentPage = currentPage.split('/')


  if( $.inArray(currentPage[1], productPages) !== -1 ) {
    $('.nav-item a').each(function() {
      var link = $(this).attr('href')
      $(this).removeClass('active')
      if( link.indexOf('/products/') !== -1 ) {
        $(this).addClass('active')
      }
    })
    $('.menu-item a').each(function() {
      var link = $(this).attr('href')
      $(this).removeClass('active')
      if( link.indexOf('/products/') !== -1 ) {
        $(this).addClass('active')
      }
    })
  }

  if( $.inArray(currentPage[1], whyPages) !== -1 ) {
    $('.nav-item a').each(function() {
      var link = $(this).attr('href')
      $(this).removeClass('active')
      if( link.indexOf('/why/') !== -1 ) {
        $(this).addClass('active')
      }
    })
    $('.menu-item a').each(function() {
      var link = $(this).attr('href')
      $(this).removeClass('active')
      if( link.indexOf('/why/') !== -1 ) {
        $(this).addClass('active')
      }
    })
  }
  if( $.inArray(currentPage[1], resourcePages) !== -1 ) {
    $('.nav-item a').each(function() {
      var link = $(this).attr('href')
      $(this).removeClass('active')
      if( link.indexOf('/resources/') !== -1 ) {
        $(this).addClass('active')
      }
    })
    $('.menu-item a').each(function() {
      var link = $(this).attr('href')
      $(this).removeClass('active')
      if( link.indexOf('/resources/') !== -1 ) {
        $(this).addClass('active')
      }
    })
  }
  
  })(jQuery);