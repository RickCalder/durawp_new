<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package     WordPress
 * @subpackage  Hockey24x7 Theme
 * @since       Hockey24x7 Theme 1.0.0
 */

if (!class_exists('Timber')) {
  echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp/wp-admin/plugins.php#timber">/wp/wp-admin/plugins.php</a>';
  return;
}
$data = Timber::get_context();
$data['posts'] = Timber::get_posts();
$data['title'] = 'Search results for: '. get_search_query();
$data['search_text'] = get_search_query();

global $paged;
if (!isset($paged) || !$paged){
    $paged = 1;
}

$data['post_count'] = $wp_query->found_posts;
$data['pagination'] = Timber::get_pagination(3);

// Templates to try to render
$templates = array('search.twig', 'index.twig');
if (isset($params['base_route'])) {
  array_unshift($templates, 'page-' . $params['base_route'] . '.twig');
} else if (is_category()) {
  array_unshift($templates, 'archive-' . get_query_var('cat') . '.twig');
} else if (is_post_type_archive()) {
  array_unshift($templates, 'archive-' . get_post_type() . '.twig');
}

$blog_ID = get_option('page_for_posts');
$data['blog_page'] = new TimberPost($blog_ID);

Timber::render($templates, $data);
