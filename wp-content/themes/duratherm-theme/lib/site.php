<?php
  class Site extends TimberSite {
    public static function register_post_types(){
    }

    public static function register_menus() {
      register_nav_menus(array(
        'primary' => 'Primary Menu',
      ));
    }

    public static function register_sidebars() {
      $before_title = '<h2 class="widget-title">';
      $after_title = '</h2>';
      $before_widget = '<div class="widget widget-type-%2$s">';
      $after_widget = '</div>';

      $sidebars = array(
        array(
          'name' => __('Before Content', 'duratherm-theme'),
          'id' => 'before_content',
          'description' => __('', 'duratherm-theme'),
          'before_title' => $before_title,
          'after_title' => $after_title,
          'before_widget' => $before_widget,
          'after_widget' => $after_widget,
        ),
        array(
          'name' => __('After Content', 'duratherm-theme'),
          'id' => 'after_content',
          'description' => __('', 'duratherm-theme'),
          'before_title' => $before_title,
          'after_title' => $after_title,
          'before_widget' => $before_widget,
          'after_widget' => $after_widget,
        ),
        array(
          'name' => __('Page Sidebar', 'duratherm-theme'),
          'id' => 'page_sidebar',
          'description' => __('', 'duratherm-theme'),
          'before_title' => $before_title,
          'after_title' => $after_title,
          'before_widget' => $before_widget,
          'after_widget' => $after_widget,
        ),
      );

      foreach ($sidebars as $sidebar) {
        register_sidebar($sidebar);
      }
    }

    public static function register_widgets() {
      $widgets = array(
        'widget' => 'WPH_Widget',
        'map' => 'nhlchat_Theme_Map_Widget',
      );

      foreach ($widgets as $slug => $classname) {
        require(__DIR__ . '/widgets/' . $slug . '.php');
        if ($slug != 'widget') {
          register_widget($classname);
        }
      }
    }

    public static function add_image_sizes() {
      add_image_size('slide', 1398, 502, $crop = true);            // Slider
      add_image_size('recent_post_large', 679, 342, $crop = true); // Posts in recent posts widget
      add_image_size('recent_post_small', 284, 189, $crop = true); // Posts in recent posts widget
      add_image_size('recent_post', 430, 322, $crop = true);       // Posts in recent posts listing
      add_image_size('body', 1040, 460, $crop = true);             // Image in page/post body
    }

    public static function register_post_type_slider(){
      register_post_type('slider', array(
        'label' => 'Sliders',
        'description' => '',
        'public' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'capability_type' => 'post',
        'map_meta_cap' => true,
        'hierarchical' => false,
        'menu_icon' => 'dashicons-controls-play',
        'rewrite' => array('slug' => 'slider', 'with_front' => true),
        'query_var' => true,
        'supports' => array('title'),
        'labels' => array(
          'name' => __('Slider', 'duratherm-theme'),
          'singular_name' => __('Slider', 'duratherm-theme'),
          'menu_name' => __('Sliders', 'duratherm-theme'),
          'add_new' => __('Add Slider', 'duratherm-theme'),
          'add_new_item' => __('Add New Slider', 'duratherm-theme'),
          'edit' => __('Edit', 'duratherm-theme'),
          'edit_item' => __('Edit Slider', 'duratherm-theme'),
          'new_item' => __('New Slider', 'duratherm-theme'),
          'view' => __('View', 'duratherm-theme'),
          'view_item' => __('View Slider', 'duratherm-theme'),
          'search_items' => __('Search Sliders', 'duratherm-theme'),
          'not_found' => __('No Sliders Found', 'duratherm-theme'),
          'not_found_in_trash' => __('No Sliders Found in Trash', 'duratherm-theme'),
          'parent' => __('Parent Slider', 'duratherm-theme'),
        )
      ));
    }

  }