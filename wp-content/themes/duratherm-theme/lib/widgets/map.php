<?php
if (!class_exists('nhlchat_Theme_Map_Widget')) {
  class nhlchat_Theme_Map_Widget extends WPH_Widget {
    function __construct() {
      $args = array(
        'label' => __('Map', '<%= themeSlug >'),
        'description' => __('', '<%= themeSlug >'),
      );

      $templates = array(
        array(
          'name' => 'Default',
          'value' => 'widget--map--default.php',
        ),
      );

      $args['fields'] = array(
        array(
          'name' => __('Latitude', '<%= themeSlug >'),
          'desc' => __('', '<%= themeSlug >'),
          'id' => 'latitude',
          'type' => 'text',
          'std' => '',
          'filter' => 'strip_tags|esc_attr',
          'class' => 'widefat',
        ),
        array(
          'name' => __('Longitude', '<%= themeSlug >'),
          'desc' => __('', '<%= themeSlug >'),
          'id' => 'longitude',
          'type' => 'text',
          'std' => '',
          'filter' => 'strip_tags|esc_attr',
          'class' => 'widefat',
        ),
        array(
          'name' => __('Zoom', '<%= themeSlug >'),
          'desc' => __('', '<%= themeSlug >'),
          'id' => 'zoom',
          'type' => 'number',
          'std' => 14,
          'filter' => 'strip_tags|esc_attr',
          'class' => 'widefat',
        ),
        array(
          'name' => __('Width', '<%= themeSlug >'),
          'desc' => __('', '<%= themeSlug >'),
          'id' => 'width',
          'type' => 'number',
          'std' => 1000,
          'filter' => 'strip_tags|esc_attr',
          'class' => 'widefat',
        ),
        array(
          'name' => __('Height', '<%= themeSlug >'),
          'desc' => __('', '<%= themeSlug >'),
          'id' => 'height',
          'type' => 'number',
          'std' => 500,
          'filter' => 'strip_tags|esc_attr',
          'class' => 'widefat',
        ),
        array(
          'name' => __('Template', '<%= themeSlug >'),
          'desc' => __('', '<%= themeSlug >'),
          'id' => 'template',
          'type' => 'select',
          'fields' => $templates,
          'std' => 0,
          'filter' => 'strip_tags|esc_attr',
          'class' => 'widefat',
        ),
      );

      $this->create_widget($args);
    }

    function widget($args, $instance) {
      $latitude = $instance['latitude'];
      $longitude = $instance['longitude'];
      $zoom = (!empty($instance['zoom'])) ? $instance['zoom'] : 14;
      $width = (!empty($instance['width'])) ? $instance['width'] : 1000;
      $height = (!empty($instance['height'])) ? $instance['height'] : 500;
      $template = locate_template($instance['template']);

      if ($latitude && $longitude && !empty($template)) {
        include $template;
      } else {
        // A default template could go here. Not necessary in this case.
      }
    }
  }
}