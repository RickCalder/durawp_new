<?php
	add_action('acf/save_post', 'import_new_data', 20);

	function import_new_data($post_id){
    global $post;
    global $wpdb;

    // Make sure we're on products page
    if ($post->post_type != 'products'){
      return;
    }

    //Make sure we're actually importing new data and not saving something else.
    $checkData = get_field('import_new_data');
    if(! isset($checkData[0]) ) {
    	return;
    }


	  $prefix = 'fluid_data_';
	  $querystr = "
		    SELECT DISTINCT meta_value 
		    FROM $wpdb->postmeta 
		    WHERE meta_key LIKE 'fluid_data' AND post_id = $post->ID
		";

		$total_rows = $wpdb->get_var( $querystr );
	  // echo $post->ID;echo '<xmp>';print_r($total_rows); die;
    $keys = array('temperature', 'density', 'kinematic_viscosity', 'dynamic_viscosity', 'thermal_conductivity', 'heat_capacity', 'vapour_pressure');
    for( $x = 0; $x <= $total_rows; $x++ ) {
	    foreach( $keys as $key ) {
	    	$to_delete = $prefix . $x . '_' . $key;
	    	$wpdb->delete('wp_postmeta', array('post_id'=> $post->ID, 'meta_key' => $to_delete));
	    	$to_delete = '_' . $prefix . $x . '_' . $key;
	    	$wpdb->delete('wp_postmeta', array('post_id'=> $post->ID, 'meta_key' => $to_delete));
	    }
	  }
  	update_post_meta($post->ID, 'fluid_data', 0);

  	$product_data = get_field('product_data_csv');
  	$file = fopen(site_url().$product_data['url'], 'r');

  	while( ($row = fgetcsv($file)) !== false ) {
  		$x = 0;
  		$insertArray = array();
  		foreach ( $keys as $key ) {
  			$insertArray[$key] = $row[$x];
  			$x++;
  		}
  		$i = add_row('fluid_data', $insertArray);
		}
		fclose($file);
	  $wpdb->delete('wp_postmeta', array('post_id'=> $post->ID, 'meta_key' => 'import_new_data'));
	  $wpdb->delete('wp_postmeta', array('post_id'=> $post->ID, 'meta_key' => '_import_new_data'));
	  $wpdb->delete('wp_postmeta', array('post_id'=> $post->ID, 'meta_key' => 'product_data_csv'));
	  $wpdb->delete('wp_postmeta', array('post_id'=> $post->ID, 'meta_key' => '_product_data_csv'));
	}
