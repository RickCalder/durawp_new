<?php
// echo __DIR__;die;
$root = $_SERVER['DOCUMENT_ROOT'] .'/content/themes/duratherm-theme/';
require __DIR__ . '/../vendor/autoload.php';
function save_product_data() {
  // $api_url = WP_BASE . '/en';
  $api_url = 'https://durathermfluids.com';


  $client = new GuzzleHttp\Client();
  $data = $client->request('GET', $api_url . '/wp-json/wp/v2/products?per_page=50')->getBody()->getContents();
  
  $products = json_decode($data);
  $new_products = array();
  $comp_products = array();
  $new_products[]['fluid_data']= array();
  $comp_products[]['fluid_data']= array();
  $new_products[]['duratherm_fluid_type'] = array();
  $i = 0;
  $y = 0;
  $z = 0;
  foreach( $products as $product ) {
    if(  $product->acf->duratherm_product[0] === "yes" ) {
      $z=0;
      $new_products[$i]['id'] = $product->id;
      $new_products[$i]['slug'] = $product->slug;
      foreach( $product->acf->duratherm_fluid_type as $prod ) {
        $new_products[$i]['duratherm_fluid_type'][$z] = $prod;
        $z++;
      }
      $new_products[$i]['seo_title'] = $product->acf->seo_title;
      $new_products[$i]['seo_description'] = $product->acf->seo_description;
      $new_products[$i]['image_alt_tag'] = $product->acf->image_alt_tag;
      $new_products[$i]['name'] = $product->title->rendered;
      $new_products[$i]['cooling_fluid'] = isset($product->acf->cooling_fluid[0]) ? 'yes' : 'no';
      $new_products[$i]['description'] = $product->content->rendered;
      $new_products[$i]['excerpt'] = $product->excerpt->rendered;
      $new_products[$i]['max_temp'] = ceil(($product->acf->max_bulk_temp - 32) * (5/9));
      $new_products[$i]['max_temp_standard'] = $product->acf->max_bulk_temp;
      $new_products[$i]['min_temp'] = ceil(($product->acf->optimal_minimum_temp - 32) * (5/9));
      $new_products[$i]['min_temp_standard'] = $product->acf->minimum_pump_temp;
      $new_products[$i]['film_temp'] = ceil(($product->acf->max_film_temp - 32) * (5/9));
      $new_products[$i]['film_temp_standard'] = $product->acf->max_film_temp;
      $new_products[$i]['flashpoint'] = ceil(($product->acf->flash_point - 32) * (5/9));
      $new_products[$i]['flashpoint_standard'] = $product->acf->flash_point;
      $new_products[$i]['food_grade'] = isset($product->acf->food_grade[0]) ? 'yes' : 'no';
      $new_products[$i]['quick_facts'] = $product->acf->quick_facts;
      $new_products[$i]['duratherm_product'] = $product->acf->duratherm_product[0];
      $new_products[$i]['chemical_composition'] = $product->acf->chemical_composition;
      $new_products[$i]['pour_point'] = ceil(($product->acf->pour_point - 32) * (5/9));
      $new_products[$i]['pour_point_standard'] = $product->acf->pour_point;
      $new_products[$i]['fire_point'] = ceil(($product->acf->fire_point - 32) * (5/9));
      $new_products[$i]['fire_point_standard'] = $product->acf->fire_point;
      $new_products[$i]['auto_ignition_temp'] = $product->acf->auto_ignition_temp !="" ? ceil(($product->acf->auto_ignition_temp - 32) * (5/9)) : "N/A";
      $new_products[$i]['auto_ignition_temp_standard'] = $product->acf->auto_ignition_temp !="" ? $product->acf->auto_ignition_temp : "N/A";
      $new_products[$i]['specific_gravity'] = $product->acf->specific_gravity != "" ? $product->acf->specific_gravity : "N/A";
      $new_products[$i]['average_molecular_weight'] = $product->acf->average_molecular_weight !="" ? $product->acf->average_molecular_weight : "N/A";
      $new_products[$i]['minimum_pump_temp'] = $product->acf->minimum_pump_temp;
      $new_products[$i]['minimum_pump_temp_standard'] = $product->acf->minimum_pump_temp;
      $new_products[$i]['optimal_minimum_temp'] = ceil(($product->acf->minimum_pump_temp - 32) * (5/9));
      $new_products[$i]['optimal_minimum_temp_standard'] = $product->acf->minimum_pump_temp;
      $new_products[$i]['thermal_expansion_coefficient'] = $product->acf->thermal_expansion_coefficient !="" ? $product->acf->thermal_expansion_coefficient : "N/A";
      $new_products[$i]['thermal_expansion_coefficient_standard'] = $product->acf->mtec !="" ? $product->acf->mtec : "N/A";
      $new_products[$i]['distillation_range_10'] = ceil(($product->acf->distillation_range_10 - 32) * (5/9));
      $new_products[$i]['distillation_range_10_standard'] = $product->acf->distillation_range_10;
      $new_products[$i]['distillation_range_90'] = ceil(($product->acf->distillation_range_90 - 32) * (5/9));
      $new_products[$i]['distillation_range_90_standard'] = $product->acf->distillation_range_90;
      $new_products[$i]['temperature_range'] = $product->acf->temperature_range;
      $new_products[$i]['viscosity_range'] = $product->acf->viscosity_range;
      $new_products[$i]['tds'] = $product->acf->tds_au_en;
      $new_products[$i]['sds'] = $product->acf->sds;
      $new_products[$i]['viscosity_104'] = $product->acf->viscosity_104;
      $new_products[$i]['excerpt_metric'] =  $product->acf->metric;
      $new_products[$i]['excerpt_standard']  =$product->acf->standard;
      // $new_products[$i]['fluid_data'] = $product->acf->fluid_data;
      $x=0;
      $fluidData = file_get_contents($product->acf->fluid_data);
      file_put_contents(__DIR__ . '/../assets/dist/data/' .$product->slug . '.csv', $fluidData);
      $newFluidData = array_map('str_getcsv', file(__DIR__ . '/../assets/dist/data/' .$product->slug . '.csv'));
      foreach($newFluidData as $item) {
        $new_products[$i]['fluid_data'][$x]['temperature'] = ceil(((int)$item[0] - 32) * (5/9));
        $new_products[$i]['fluid_data'][$x]['density'] = round((double)$item[1] * 0.016018463, 3);
        $new_products[$i]['fluid_data'][$x]['dynamic_viscosity'] = round($item[3], 2);
        $new_products[$i]['fluid_data'][$x]['viscosity'] = round($item[3], 2);
        $new_products[$i]['fluid_data'][$x]['kinematic_viscosity'] = round($item[2], 2);
        $new_products[$i]['fluid_data'][$x]['thermal_conductivity'] = round((double)$item[4] * 01.73, 3);
        $new_products[$i]['fluid_data'][$x]['heat_capacity'] = round((double)$item[5] * 4.1868, 3);
        $new_products[$i]['fluid_data'][$x]['vapour_pressure'] = round((double)$item[6] * 6.89476, 3);
        $new_products[$i]['fluid_data_standard'][$x]['temperature'] = $item[0];
        $new_products[$i]['fluid_data_standard'][$x]['density'] = $item[1];
        $new_products[$i]['fluid_data_standard'][$x]['dynamic_viscosity'] =$item[3];
        $new_products[$i]['fluid_data_standard'][$x]['viscosity'] = $item[3];
        $new_products[$i]['fluid_data_standard'][$x]['kinematic_viscosity'] = $item[2];
        $new_products[$i]['fluid_data_standard'][$x]['thermal_conductivity'] = $item[4];
        $new_products[$i]['fluid_data_standard'][$x]['heat_capacity'] = $item[5];
        $new_products[$i]['fluid_data_standard'][$x]['vapour_pressure'] = $item[6];
        $x++;
        // echo '<xmp>';print_r($new_products);die;
      }
      $i++;
    } else {
      // echo $product->title->rendered . '<br>';die;
      $comp_products[$y]['id'] = $product->id;
      $comp_products[$y]['name'] = $product->title->rendered;
      $comp_products[$y]['cooling_fluid'] = isset($product->acf->cooling_fluid[0]) ? 'yes' : 'no';
      if($product->acf->max_bulk_temp) {
        $comp_products[$y]['max_temp'] = ceil(($product->acf->max_bulk_temp - 32) * (5/9));
      }
      
      if($product->acf->optimal_minimum_temp != '') {
        $comp_products[$y]['min_temp'] = ceil(($product->acf->optimal_minimum_temp - 32) * (5/9));
      }
      $comp_products[$y]['max_temp_standard'] = $product->acf->max_bulk_temp;
      $comp_products[$y]['min_temp_standard'] = $product->acf->minimum_pump_temp;
      $comp_products[$y]['film_temp'] = "N/A";
      $comp_products[$y]['film_temp_standard'] = "N/A";
      $comp_products[$y]['flashpoint'] = ceil(($product->acf->flash_point - 32) * (5/9));
      $comp_products[$y]['flashpoint_standard'] = $product->acf->flash_point;
      $comp_products[$y]['food_grade'] = isset($product->acf->food_grade[0]) ? 'yes' : 'no';
      $comp_products[$y]['duratherm_product'] = "no";
      $comp_products[$y]['chemical_composition'] = $product->acf->chemical_composition;
      if($product->acf->pour_point != ''){
        $comp_products[$y]['pour_point'] = ceil(($product->acf->pour_point - 32) * (5/9));
        $comp_products[$y]['pour_point_standard'] = $product->acf->pour_point;
      }
      $comp_products[$y]['fire_point'] = $product->acf->fire_point !="" ? ceil(($product->acf->fire_point - 32) * (5/9)) : "N/A";
      $comp_products[$y]['fire_point_standard'] = $product->acf->fire_point !="" ? $product->acf->fire_point : "N/A";
      $comp_products[$y]['auto_ignition_temp'] = $product->acf->auto_ignition_temp !="" ? ceil(($product->acf->auto_ignition_temp - 32) * (5/9)) : "N/A";
      $comp_products[$y]['auto_ignition_temp_standard'] = $product->acf->auto_ignition_temp !="" ? $product->acf->auto_ignition_temp : "N/A";
      $comp_products[$y]['specific_gravity'] = $product->acf->specific_gravity != "" ? $product->acf->specific_gravity : "N/A";
      $comp_products[$y]['average_molecular_weight'] = $product->acf->average_molecular_weight !="" ? $product->acf->average_molecular_weight : "N/A";
      $comp_products[$y]['minimum_pump_temp'] = $product->acf->minimum_pump_temp;
      $comp_products[$y]['optimal_minimum_temp'] = $product->acf->optimal_minimum_temp;
      $comp_products[$y]['thermal_expansion_coefficient'] = $product->acf->thermal_expansion_coefficient !="" ? $product->acf->thermal_expansion_coefficient : "N/A";
      if($product->acf->distillation_range_10 != '') {
        $comp_products[$y]['distillation_range_10'] = ceil(($product->acf->distillation_range_10 - 32) * (5/9));
        $comp_products[$y]['distillation_range_90'] = ceil(($product->acf->distillation_range_90 - 32) * (5/9));
        $comp_products[$y]['distillation_range_10_standard'] = $product->acf->distillation_range_10;
        $comp_products[$y]['distillation_range_90_standard'] = $product->acf->distillation_range_90;
      }
      $comp_products[$y]['temperature_range'] = $product->acf->temperature_range;
      $comp_products[$y]['viscosity_range'] = $product->acf->viscosity_range;
      $comp_products[$y]['viscosity_104'] = $product->acf->viscosity_104;
      $x=0;
      $fluidData = file_get_contents($product->acf->fluid_data);
      // echo '<xmp>'; print_r($fluidData);die;
      file_put_contents(__DIR__ . '/../assets/dist/data/' .$product->slug . '.csv', $fluidData);
      $newFluidData2 = array_map('str_getcsv', file(__DIR__ . '/../assets/dist/data/' .$product->slug . '.csv'));
      foreach($newFluidData2 as $item) {
        $comp_products[$y]['fluid_data'][$x]['temperature'] = ceil(((int)$item[0] - 32) * (5/9));
        $comp_products[$y]['fluid_data'][$x]['density'] = $item[1] != "" ? round($item[1] * 0.016018463, 3) : "N/A";
        $comp_products[$y]['fluid_data'][$x]['dynamic_viscosity'] = $item[3] != "" ? round($item[3], 2) : "N/A";
        $comp_products[$y]['fluid_data'][$x]['viscosity'] = $item[3] != "" ? round($item[3], 2) : "N/A";
        $comp_products[$y]['fluid_data'][$x]['kinematic_viscosity'] = $item[2] != "" ? round($item[2], 2) : "N/A";
        $comp_products[$y]['fluid_data'][$x]['thermal_conductivity'] = $item[4] != "" ? round($item[4] * 01.73, 3) : "N/A";
        $comp_products[$y]['fluid_data'][$x]['heat_capacity'] = $item[5] != "" ? round($item[5] * 4.1868, 3) : "N/A";
        $comp_products[$y]['fluid_data'][$x]['vapour_pressure'] = $item[6] != "" ? round($item[6] * 6.89476, 3) : "N/A";
        $comp_products[$y]['fluid_data_standard'][$x]['temperature'] = $item[0];
        $comp_products[$y]['fluid_data_standard'][$x]['density'] = $item[1];
        $comp_products[$y]['fluid_data_standard'][$x]['dynamic_viscosity'] =$item[3];
        $comp_products[$y]['fluid_data_standard'][$x]['viscosity'] = $item[3];
        $comp_products[$y]['fluid_data_standard'][$x]['kinematic_viscosity'] = $item[2];
        $comp_products[$y]['fluid_data_standard'][$x]['thermal_conductivity'] = $item[4];
        $comp_products[$y]['fluid_data_standard'][$x]['heat_capacity'] = $item[5];
        $comp_products[$y]['fluid_data_standard'][$x]['vapour_pressure'] = $item[6];
        $x++;
      }
      $y++;
    }
  }

  $tempProducts = array();
  foreach ($new_products as $key => $row) {
      $tempProducts[$key] = $row['max_temp'];
  }
  array_multisort($tempProducts, SORT_DESC, $new_products);

  if( !empty($comp_products) && isset($comp_products[0]['id'])) {
    $tempCompProducts = array();
    foreach ($comp_products as $key => $row) {
        $tempCompProducts[$key] = $row['name'];
    }
    array_multisort($tempCompProducts, SORT_ASC, $comp_products);
  }

// // echo '<xmp>'; print_r($new_products); die;
  //Save the WP content to a data.json file.
  file_put_contents(__DIR__ . '/../assets/dist/data/products' .$lang . '.json', json_encode($new_products));
  file_put_contents(__DIR__ . '/../assets/dist/data/comp_products' .$lang . '.json', json_encode($comp_products));

}