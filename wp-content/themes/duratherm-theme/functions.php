<?php
require_once __DIR__ . '/lib/acf.php';
require_once __DIR__ . '/lib/save-products.php';
require_once __DIR__ . '/lib/create_products.php';
  // save_product_data();
function update_product_json() {
  save_product_data();
}
add_action('save_post_products', 'update_product_json');
add_filter('use_block_editor_for_post', '__return_false');
add_filter('use_block_editor_for_page', '__return_false');

if (class_exists('Timber')) {
  require_once __DIR__ . '/lib/site.php';
}

add_theme_support('menus');
add_theme_support('post-thumbnails');
add_action('init', 'register_custom_posts_init');

add_action('wp', function() {
  if( 'products' === get_post_type() ) {
    add_filter('wpseo_json_ld_output', 'bybe_remove_yoast_json', 10, 1);
  }
});
function bybe_remove_yoast_json($data){
  $data = array();
  return $data;
}

// Register Custom Post Types

function register_custom_posts_init() {
  // Register Products
  $products_labels = array(
    'name'               => 'Products',
    'singular_name'      => 'Product',
    'menu_name'          => 'Products'
  );
  $products_args = array(
    'public'             => true,
    'show_in_rest'       => true,
    'labels'             => $products_labels,
    'public'             => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'supports'           => array( 'title', 'editor', 'thumbnail', 'revisions', 'excerpt' )
  );

  // Register Testimonials
  $testimonials_labels = array(
    'name'               => 'Testimonials',
    'singular_name'      => 'Testimonial',
    'menu_name'          => 'Testimonials'
  );
  $testimonials_args = array(
    'public'             => true,
    'show_in_rest'       => true,
    'labels'             => $testimonials_labels,
    'public'             => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'supports'           => array( 'title', 'editor', 'revisions' )
  );

  // Register Distributors
  $distributors_labels = array(
    'name'               => 'Distributors',
    'singular_name'      => 'Distributor',
    'menu_name'          => 'Distributors'
  );
  $distributors_args = array(
    'public'             => true,
    'show_in_rest'       => true,
    'labels'             => $distributors_labels,
    'public'             => true,
    'capability_type'    => 'post',
    'has_archive'        => true,
    'supports'           => array( 'title', 'editor', 'revisions' )
  );

  register_post_type('products', $products_args);
  register_post_type('testimonials', $testimonials_args);
  register_post_type('distributors', $distributors_args);
}

add_action('rest_api_init', 'register_rest_images' );
function register_rest_images(){
    register_rest_field( array('post'),
        'fimg_url',
        array(
            'get_callback'    => 'get_rest_featured_image',
            'update_callback' => null,
            'schema'          => null,
        )
    );
}
function get_rest_featured_image( $object, $field_name, $request ) {
    if( $object['featured_media'] ){
        $img = wp_get_attachment_image_src( $object['featured_media'], 'app-thumb' );
        return $img[0];
    }
    return false;
}

add_filter('timber_context', function($data) {
  $data['menu'] = new TimberMenu('primary');
  $data['options'] = get_fields('options');
  $data['before_content'] = Timber::get_widgets('before_content');
  $data['after_content'] = Timber::get_widgets('after_content');
  $data['page_sidebar'] = Timber::get_widgets('page_sidebar');
  $data['post_sidebar'] = Timber::get_widgets('post_sidebar');

  return $data;
});


if (function_exists('acf_add_options_page')) {
  acf_add_options_page('Site Info');
}

function load_script($name, $src, $footer = true) {
  wp_register_script($name, $src, array(), '1.12', $footer);
  wp_enqueue_script($name);
}

function load_style($name, $src, $media = 'all') {
  wp_register_style($name, $src, false, false, $media);
  wp_enqueue_style($name);
}

function load_styles() {
  load_style('duratherm-theme', get_stylesheet_directory_uri() . '/assets/dist/css/main.min.css', 'screen');
  load_style('duratherm-theme-print', get_stylesheet_directory_uri() . '/assets/dist/css/print.min.css', 'print');
}
add_action('wp_enqueue_scripts', 'load_styles');

if (!is_admin()) add_action("wp_enqueue_scripts", "my_jquery_enqueue", 11);
function my_jquery_enqueue() {
   wp_deregister_script('jquery');
   wp_register_script('jquery', "//code.jquery.com/jquery-3.2.1.min.js", null, false, true);
   wp_register_script('popper', "//cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js", array('jquery'), false, true);
   wp_register_script('bootstrap', "//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js", array('jquery'), false, true);
   wp_register_script('jValidate', "//cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js", array('jquery'), false, true);
   wp_register_script('jqueryUI', "//cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js", array('jquery'), false, true);
   wp_register_script('underscore', "//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.9.0/underscore-min.js", array(), false, true);
   wp_enqueue_script('jquery');
   wp_enqueue_script('popper');
   wp_enqueue_script('jValidate');
   wp_enqueue_script('bootstrap');
   wp_enqueue_script('jqueryUI');
   wp_enqueue_script('underscore');
}

function load_footer_scripts() {
  load_script('duratherm-theme', get_stylesheet_directory_uri() . '/assets/dist/js/main.min.js?v=1', true);
}

function asset_path($path) {
  return get_stylesheet_directory_uri() . '/assets/dist/' . $path;
}

if (class_exists('Site')) {
  Site::register_sidebars();

  add_action('init', function() {
    Site::register_menus();
    Site::add_image_sizes();
    Site::register_post_types();
  });

  add_action('widgets_init', function() {
    Site::register_widgets();
  });
}


//remove WP Emoji

function disable_wp_emojicons() {

  // all actions related to emojis
  remove_action( 'admin_print_styles', 'print_emoji_styles' );
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );

  // filter to remove TinyMCE emojis
  add_filter( 'tiny_mce_plugins', 'disable_emojicons_tinymce' );
}
add_action( 'init', 'disable_wp_emojicons' );

function disable_emojicons_tinymce( $plugins ) {
  if ( is_array( $plugins ) ) {
    return array_diff( $plugins, array( 'wpemoji' ) );
  } else {
    return array();
  }
}
function wpse_allowedtags() {
  // Add custom tags to this string
      return '<script>,<style>,<br>,<em>,<i>,<ul>,<ol>,<li>,<a>,<p>,<img>,<video>,<audio>'; 
  }

if ( ! function_exists( 'wpse_custom_wp_trim_excerpt' ) ) {

function wpse_custom_wp_trim_excerpt($wpse_excerpt) {
  $raw_excerpt = $wpse_excerpt;
  if ( '' == $wpse_excerpt ) {

    $wpse_excerpt = get_the_content('');
    $wpse_excerpt = strip_shortcodes( $wpse_excerpt );
    $wpse_excerpt = apply_filters('the_content', $wpse_excerpt);
    $wpse_excerpt = str_replace(']]>', ']]&gt;', $wpse_excerpt);
    $wpse_excerpt = strip_tags($wpse_excerpt, wpse_allowedtags()); /*IF you need to allow just certain tags. Delete if all tags are allowed */

    //Set the excerpt word count and only break after sentence is complete.
    $excerpt_word_count = 75;
    $excerpt_length = apply_filters('excerpt_length', $excerpt_word_count); 
    $tokens = array();
    $excerptOutput = '';
    $count = 0;

      // Divide the string into tokens; HTML tags, or words, followed by any whitespace
      preg_match_all('/(<[^>]+>|[^<>\s]+)\s*/u', $wpse_excerpt, $tokens);

      foreach ($tokens[0] as $token) { 
        if ($count >= $excerpt_length && preg_match('/[\,\;\?\.\!]\s*$/uS', $token)) { 
          // Limit reached, continue until , ; ? . or ! occur at the end
          $excerptOutput .= trim($token);
          break;
        }
          // Add words to complete sentence
          $count++;
          // Append what's left of the token
          $excerptOutput .= $token;
        }

      $wpse_excerpt = trim(force_balance_tags($excerptOutput));

      $excerpt_end = ' <a href="'. esc_url( get_permalink() ) . '">' . '&nbsp;&raquo;&nbsp;' . sprintf(__( 'Read more about: %s &nbsp;&raquo;', 'wpse' ), get_the_title()) . '</a>'; 
      $excerpt_more = apply_filters('excerpt_more', ' ' . $excerpt_end); 

      //$pos = strrpos($wpse_excerpt, '</');
      //if ($pos !== false)
      // Inside last HTML tag
      //$wpse_excerpt = substr_replace($wpse_excerpt, $excerpt_end, $pos, 0); /* Add read more next to last word */
      //else
      // After the content
      $wpse_excerpt .= $excerpt_more; /*Add read more in new paragraph */
      // die($wpse_excerpt);
      return $wpse_excerpt;   

    }
    return apply_filters('wpse_custom_wp_trim_excerpt', $wpse_excerpt, $raw_excerpt);
  }
}
remove_filter('get_the_excerpt', 'wp_trim_excerpt');
add_filter('get_the_excerpt', 'wpse_custom_wp_trim_excerpt');

add_action('init', 'myStartSession', 1);
add_action('wp_logout', 'myEndSession');
add_action('wp_login', 'myEndSession');

function myStartSession() {
  if(!session_id()) {
      session_start();
  }
}

function myEndSession() {
  session_destroy ();
}

// Contact Form

// if you want none logged in users to access this function use this hook
add_action('wp_ajax_nopriv_mail_before_submit', 'duratherm_send_mail_before_submit');

// if you want both logged in and anonymous users to get the emails, use both hooks above
function duratherm_send_mail_before_submit(){
  saveContactData($_POST);
    check_ajax_referer('duratherm-string','_ajax_nonce');
    if ( isset($_POST['action']) && $_POST['action'] == "mail_before_submit" ){
      //send email  wp_mail( $to, $subject, $message, $headers, $attachments ); ex:
      if($_POST['withquote'] === 'true' ) {
        $subject = 'Request for Quote from Duratherm.com';
      } else {
        $subject = 'Request for Contact from Duratherm.com';
      }
      $headers[]   = 'Reply-To: '.$_POST['toemail'] .' <' . $_POST['toemail'] .'>';

      $email = get_field('email', 'option');

      wp_mail($email, $subject, $_POST['email_body'], $headers);
      sendClientMail($_POST['toemail']);
      echo 'email sent';
      die();
    }
    echo 'error';
    die();
}

function sendClientMail($email){
  $subject = 'Request received - thanks for your interest in Duratherm fluids';
  $body = '<!doctype html>
  <html>
    <head>
      <meta name="viewport" content="width=device-width">
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <title>Thank you for contacting us!</title>
   
    </head>
    <body class="" style="background-color: #f6f6f6; font-family: sans-serif; -webkit-font-smoothing: antialiased; font-size: 14px; line-height: 1.4; margin: 0; padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;">
      <table border="0" cellpadding="0" cellspacing="0" class="body" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background-color: #f6f6f6;">
        <tr>
          <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
          <td class="container" style="font-family: sans-serif; font-size: 14px; vertical-align: top; display: block; Margin: 0 auto; max-width: 720px; padding: 10px; width: 720px;">
            <div class="content" style="box-sizing: border-box; display: block; Margin: 0 auto; max-width: 720px; padding: 10px;">
  
              <span class="preheader" style="color: transparent; display: none; height: 0; max-height: 0; max-width: 0; opacity: 0; overflow: hidden; mso-hide: all; visibility: hidden; width: 0;">This is preheader text. Some clients will show this text as a preview.</span>
              <table class="main" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; background: #ffffff; border-radius: 3px;">
  
                <tr>
                  <td class="wrapper" style="font-family: sans-serif; font-size: 14px; vertical-align: top; box-sizing: border-box; padding: 20px;">
                    <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                      <tr>
                          <td style="background-color:#24408e; width:65%; height: 80px">
                            <p style="padding:15px 30px; font-size:1.5em; font-weight:bold; color:#fff; text-align:center;">
                              Thanks for contacting us!
                            </p>
                          </td>
                          <td style="width: 30%; padding: 10px; text-align: center">
                            <img style="width: 200px" src="https://durathermfluids.com/wp-content/themes/duratherm-theme/assets/dist/img/duratherm_email.png" alt="duratherm logo">
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding: 0 30px">
                          <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px; margin-top: 50px;">We\'ll get back to you right away - usually within one business day.</p>
                          <p style="font-family: sans-serif; font-size: 14px; font-weight: normal; margin: 0; Margin-bottom: 15px;">If we can be of any further assistance in the meantime, please contact us at 1-800-446-4910 or reply directly to this email.</p>
                          <table border="0" cellpadding="0" cellspacing="0" class="btn btn-primary" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%; box-sizing: border-box;">
                            <tbody>
                              <tr>
                                <td align="left" style="font-family: sans-serif; font-size: 14px; vertical-align: top; padding-bottom: 15px;">
                                  <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: auto;">
                                    <tbody>
                                    </tbody>
                                  </table>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                          <table>
                            <tbody>
                              <tr>
                                <td colspan="2" style="background-color: #f8fafe; border: 1px solid #ccc; margin: 20px; padding: 20px">
                                    <p style="font-size: 1.25em; font-weight:bold; margin: 0; background-color: #f8fafe; margin-bottom: 10px">Did you know?</p>
                                    <p style="font-family: sans-serif; font-size: 14px; font-weight: normal;  margin: 0; background-color: #f8fafe;">
                                        We offer some of the industry\'s best <a href="https://durathermfluids.com/heat-transfer-fluid/">heat transfer fluids</a> and <a href="https://durathermfluids.com/system-cleaners/">system cleaners</a>, but we\'ve built our business on great service. With added benefits like <a href="https://durathermfluids.com/why/analysis/">complimentary fluid analysis</a>, quick shipping and no-charge tech support, it\'s little wonder our customers have such <a href="https://durathermfluids.com/why/testimonials/">great things</a> to say about us.
                                    </p>
                                </td>
                              </tr>
                            </tbody>
                          </table>
                        </td>
                      </tr>
                    </table>
                  </td>
                </tr>
              </table>
              <div class="footer" style="clear: both; Margin-top: 10px; text-align: center; width: 100%;">
                <table border="0" cellpadding="0" cellspacing="0" style="border-collapse: separate; mso-table-lspace: 0pt; mso-table-rspace: 0pt; width: 100%;">
                  <tr>
                    <td class="content-block" style="font-family: sans-serif; vertical-align: top; padding-bottom: 10px; padding-top: 10px; font-size: 12px; color: #999999; text-align: center;">
                      <span class="apple-link" style="color: #999999; font-size: 12px; text-align: center;"><a href="https://durathermfluids.com">Duratherm Fluids</a></span>
                    </td>
                  </tr>
                </table>
              </div>
            </div>
          </td>
          <td style="font-family: sans-serif; font-size: 14px; vertical-align: top;">&nbsp;</td>
        </tr>
      </table>
    </body>
  </html>';
  $headers = array('Content-Type: text/html; charset=UTF-8');
  wp_mail($email, $subject, $body, $headers);
  echo 'email sent';
  die();
}

function saveContactData($post) {
  if($post['withquote'] === 'true' ) {
    $subject = 'Quote';
  } else {
    $subject = 'Contact';
  }
  global $wpdb;
  $wpdb->insert(
    'contact_forms', 
    array( 
      'from_email' => $post['toemail'], 
      'subject' => $subject,
      'body' => $post['email_body'] 
    ) 
  );
  return;
}

/**
 * Remove WPML hreflangs entirely
 */
add_filter('wpml_hreflangs', 'custom_lang_code', 10, 1);
function custom_lang_code($hreflang_items){
  unset ($hreflang_items);
}

/* Remove posts from sitemap.xml */
add_filter( 'wpseo_exclude_from_sitemap_by_post_ids', function () {
  return array( 387, 391, 411, 403, 407, 427, 395, 399, 423, 419, 415, 435, 431, 389, 393, 413, 405, 409, 429, 397, 401, 425, 421, 417, 437, 433, 340, 341, 346, 345, 347, 211, 342, 343, 253, 339, 344, 79, 203, 388, 392, 412, 404, 408, 428, 396, 400, 424, 420, 416, 436, 432, 386, 390, 410, 402, 406, 426, 394, 398, 422, 418, 414, 434, 440 );
} );


add_action( 'admin_menu', 'my_admin_menu' );
function my_admin_menu() {
	add_menu_page( 'Contact Submissions', 'Contact Submissions', 'manage_options', 'myplugin/myplugin-admin-page.php', 'myplguin_admin_page', 'dashicons-tickets', 6  );
}

function myplguin_admin_page(){
  global $wpdb;
  $contacts = $wpdb->get_results("SELECT * FROM contact_forms ORDER BY ID DESC;");
	?>
	<div class="wrap">
    <h2>Contact Form Submissions</h2>
    <style>
      td { padding: 0.5rem; font-size: 1rem; border: 1px solid #ccc;}
      table {width: 80%}
      th{ font-size: 1.25rem; font-weight: bold; border: 1px solid #ccc; padding: 10px;}
      .message-body{ display: none; }
    </style>
    <table class="table">
      <thead>
        <tr>
          <th>From email</th>
          <th>Received</th>
          <th>Subject</th>
          <th>Message</th>
          <th></th>
          </tr>
      </thead>
      <tbody>
    <?php 
      $x = 0;
      foreach($contacts as $contact ) {
    ?>
    <tr>
        <td><?php echo $contact->from_email; ?></td>
        <td><?php echo $contact->created_at; ?></td>
        <td><?php echo $contact->subject; ?></td>
        <td><div class="message-body message-<?php echo $x ?>"><?php echo nl2br($contact->body); ?></div></td>
        <td><a href="#" class="column-toggle" data-column="<?php echo $x; ?>">Expand/Collapse</a></td>
    </tr>
    <?php
    $x++;
      }
    ?>
    </tbody>
    </table>
  </div>
  <script>
    jQuery(".column-toggle").on("click", function(e){
      e.preventDefault()
      var column = jQuery(this).data('column')
      jQuery(".message-" + column ).slideToggle()
    })
  </script>
	<?php
}