<?php require_once($_SERVER['DOCUMENT_ROOT'] . '/config.php'); ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<title>Heat Transfer Fluids, Thermic Fluid, Heat Transfer Thermal Oil, Lubricants</title>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="title" content="Heat Transfer Fluids, Thermic Fluid, Heat Transfer Thermal Oil, Lubricants" />
<meta name="Keywords" content="thermic fluid, heat transfer fluids, heat transfer fluid, heat transfer oil, thermal fluid, thermal oil"/>
<meta name="Description" content="For over 25 years Duratherm has been servicing the heat transfer industry with quality, effective and innovative products including thermic fluids, thermal oils and heat transfer related lubricants." />

<link rel="stylesheet" type="text/css" href="includes/css/text.css" media="screen,print" />
<link rel="stylesheet" type="text/css" href="includes/css/print.css" media="print" />
<link rel="stylesheet" type="text/css" href="includes/css/screen_new.css" media="screen" />

<link rel="alternate stylesheet" type="text/css" href="includes/css/text-large.css" title="large" media="screen"/>
<link rel="alternate stylesheet" type="text/css" href="includes/css/text-larger.css" title="larger" media="screen"/>

<script src="includes/javascript/default.js" type="text/javascript"></script>

<style type="text/css">

#pageBody div.content div {
	padding: 10px 0 0 10px;
	margin: 0 0 20px 0;
	border-top: 1px solid #e4e4e4;
	background: url(../../images/content_corner_left.gif) #ffffff no-repeat left top;
}

#pageBody div.content div p {
	padding: 0;
	margin: 5px 0 10px 3px;
}

#pageBody div.sidebar div {
	border-bottom: 1px solid #dddddd;
}

#pageBody div.sidebar div.nav {
	border: 0px solid #dddddd;
	padding-bottom: 20px;
}

#pageBody div.sidebar h2,
#pageBody div.sidebar h3 {
	border-bottom: 1px dotted #dddddd;
	font-size: 160%;
	font-weight: normal;
}

#pageBody div.sidebar h2 {
	margin: 1em 0 0em;
}

#pageBody div.sidebar h4 {
	margin: 0.5em 0 0.35em;
	color: #333333;
	font-size: 150%;
	font-weight: normal;
}

#pageBody div.sidebar p.expander a {
	width: 100%;
	display: block;
}

#pageBody div.sidebar p.expander a:link,
#pageBody div.sidebar p.expander a:visited {
	background: #ffffff;
}

#pageBody div.sidebar p.expander a:hover,
#pageBody div.sidebar p.expander a:active {
	background: url(images/nav/main/expanding_hov.gif) #084195 no-repeat right center;
}

#pageFooter p {
	width: 74%;
	margin: 0;
	float: left;
}

#pageFooter ul {
	margin: 0;
	float: right;
	width: 24%;
	list-style-type: none;
	text-align: right;
}

</style>

</head>
<body> 

<div id="pageContainer"> 
	
	<div id="pageHeader"> 
			
		<div class="logo"> 
			<h1><a href="/" title="heat-transfer-fluid.com"><span>Duratherm</span></a></h1> 
		</div> 
			
			<div class="accessibility"> 
				<p><a href="#mainNav">Skip to Navigation</a> | <a href="#mainContent" title="[2] Skip to Content" accesskey="2">Skip to Content</a></p> 
				<hr /> 
		</div> 
		
			<div class="nav"> 
				<ul> 
					<li id="homeLink"><a href="/" title="[1] Home" accesskey="1" >Home</a></li> 
					<li id="productLink"><a href="/products/" title="Duratherm Products" >Products</a></li> 
					<li id="whyLink"><a href="/why/" title="Why Choose Duratherm">Why Duratherm</a></li> 
					<li id="techLink"><a href="/resources/" title="Technical Resources">Technical Resources</a></li> 
					<li id="abouthtfLink" <? if (strstr(CURRENT_DIR, '/abouthtf')) echo 'class="active"' ?>> <a href="/abouthtf/" title="About Heat Transfer Fluids">About Heat Transfer Fluids</a></li>
					<li id="contactLink"><a href="/contact.php" title="[9] Address and contact information" accesskey="9">Contact</a></li> 
				</ul> 
		</div> 
			
	</div> 
		
		<div id="pageBody"> 
		
			<div class="sidebar">
				<?php
					echo $nav->getNav(
						$products 	= true,
						$htfs 		= false,
						$cleaners 	= false,
						$lubricants = false,
						$why 		= false,
						$resources  = false,
						$abouthft= false
					);
					?>
			</div>
			
			<div id="mainContent" class="content"> 
			
				<div> 
					<h1>Who We Are...</h1>
					<p>For over 25 years we have been servicing the heat transfer fluid industry with quality, effective and innovative <a href="products/index.php" title="Link to the Products Section">products</a> ranging from heat transfer fluid and related products, thermal oils, thermic fluids as well as specialized lubricants throughout the world.</p>
					<p>Working closely with equipment manufacturers and our customers we have gained an intimate and unrivaled understanding of real world concerns for
heat transfer fluids, including pioneering the development and use of system cleaners as a maintenance tool.</p>
				</div>
				
				<div> 
					<h1>What We Do...</h1> 										
					<p>We offer the industries most expansive line of <a href="products/htf/index.php" title="Duratherm's Heat Transfer Fluid Products">heat transfer fluids</a>. <a href="products/compatibility.php" title="About Compatibility">Compatibility</a> with any system and heat transfer fluid is now achievable from a single vendor. Thermic fluids with operating temperatures from -50&deg;F to 650&deg;F all while retaining an environmental responsibility as well as unique and innovative <a href="products/cleaners/index.php" title="Duratherm's System Cleaner Products">system cleaners</a> make us your single source for all aspects of heat transfer fluids (also called hot oils, thermic fluids or heat transfer oil).</p>
					<p>Free technical support. Regardless of heat transfer fluid currently in use, <a href="contact.php" title="Link to the Contact Page" accesskey="9">contact us</a> for technical support including a complimentary fluid analysis.</p>
				</div> 
				
				<div> 
					<h1>What Makes Us Different?</h1>					
					<p>Our world class customer service would be pointless without <a href="why/index.php" title="Link to the Why Duratherm section">cutting edge heat transfer fluids</a>, shipped same day from one of our <a href="contact.php" title="Link to the Contact page">convenient 
					warehouses</a>. All heat transfer fluids are available in as small as 5 gallon pails and all major credit cards are accepted for quick and easy order processing.</p>
				</div>
				
				<a href="sitemap.html"><img src="sitemaplink.gif" alt="" border="0"/></a>
				 
			</div> 
			
			<div class="sidebar">
				<h2>What About You?</h2> 
				
				<h4>Are You...</h4> 
				
				<p class="expander"><strong><a href="javascript:;" title="Show or Hide Equipment Manufacturer Information" onclick="toggleElement('oems'); return false;">
				An Equipment Manufacturer?</a></strong></p> 
				
					<div id="oems" style="display:none;"> 
						<p><strong>Do you manufacturer equipment that uses a heat transfer fluid?</strong><br /> 
						<a href="contact.php" title="Link to the Contact page">Contact us</a> about our OEM partnerships that encompass all aspects of heat transfer fluids beginning with design and engineering assistance, fluid support and specialty application development as well as after sale customer services and support.</p>
					</div> 
				
				<p class="expander"><a href="javascript:;" title="Show or Hide Distibutor Information" onclick="toggleElement('distributors'); return false;"><strong>
				A Distributor?</strong></a></p> 
				
					<div id="distributors" style="display:none;"> 
						<p><strong>We are always looking to expand our Duratherm team.</strong><br /> 
						If you are a reseller or distributor <a href="contact.php" title="Link to the Contact page">contact us</a> to discuss how we can work together.</p> 
					</div> 
				
				<p class="expander"><a href="javascript:;" title="Show or Hide Student Information" onclick="toggleElement('students'); return false;"><strong>
				A Student?</strong></a></p> 
				
					<div id="students" style="display:none;"> 
						<p><strong>Are you a Student Doing Research?</strong><br /> 
						We always do what we can to further the understanding and use of heat transfer fluids. Feel free to <a href="contact.php" title="Link to the Contact page">contact one of our departments</a> for answers to any questions you may have. We welcome student enquiries.</p> 
					</div> 
				
				<p class="expander"><a href="javascript:;" title="Show or Hide Customer Information" onclick="toggleElement('customers'); return false;"><strong>
				A Customer?</strong></a></p> 
				
					<div id="customers" style="display:none;"> 
						<p>We invite you to freely browse our web site but of course should you require a more personalized recommendation we would be happy to <a href="contact.php" title="Link to the Contact page">contact</a> you by a method of your choice - email, phone, fax or of course a personal in-plant visit.</p> 
					</div>
					 
			</div> 
			
			<br class="bottom" /> 
			
	</div> 
		
		<div id="pageFooter"> 
			<p>Duratherm Extended Life Fluids is a division of Frontier Resource and Recovery Services Incorporated.</p>
    <ul> 
				<li class="last"><a href="/contact.php" title="Link to the Contact page.">Contact</a></li> 
				<li><a href="products/" title="Jump to our Products Section">Products</a></li> 
				<li><a href="/aboutsite.php" title="Link to a page describing this site's CSS, XHTML, and Accessibility compliances">About Site</a></li> 
			</ul> 
			<p id="credits"><a href="http://shiftmediagroup.com/" title="Link to shiftmediagroup.com">Web Design and Development by Shift Media Group</a> | 905 641 2553</p> 
		
		</div> 
		
	</div>
 
	<!-- StatCounter -->
	
	<script type="text/javascript" language="javascript">
		var sc_project=310463; 
		var sc_partition=1; 
		var sc_invisible=1; 
	</script>
	
	<script type="text/javascript" language="javascript" src="http://www.statcounter.com/counter/frames.js">
	</script>

	<noscript>
		<a href="http://www.statcounter.com/free_hit_counter.html" title="Link to statcounter.com" onclick="window.open(this.href, '_blank', 'width=675,height=300,left=75,top=200,resizable=yes'); return false;">html hit counter</a>
	</noscript>
</body>
</html>
